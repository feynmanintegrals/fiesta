# README #

This program is distributed under the terms of the [GPU GPLv3 licence](license.md)

FIESTA stands for Feynman Integral Evaluation by a Sector decomposiTion Approach.

### Articles ###

* [FIESTA1](http://arxiv.org/abs/0807.4129) (basic version)
* [FIESTA2](http://arxiv.org/abs/0912.0158) (mpfr)
* [FIESTA3](http://arxiv.org/abs/1312.3186) (physical regions, databases, mpi)
* [FIESTA4](http://arxiv.org/abs/1511.03614) (vectorization, gpu)
* [FIESTA5](http://arxiv.org/abs/2110.11660) (multiple optimizations, new integrators, avx)

Papers about FIESTA1-4 were published in Computer Physics Communications, we expect to publish FIESTA5 there as well.

### Installation ###

* git clone https://bitbucket.org/feynmanIntegrals/fiesta.git
* cd fiesta/FIESTA5
* ./configure (you can add the --enable-tt and/or --enable-qmc options to build extra integrators)
* if you are planning to use math binary different from "math" in your work, add --math=\<MATHBINARY\> to configure options
* --enable-malloc switches the memory allocator)
* make dep (as usual, add -j for speed)
* make
* make test (just a few basic tests)
* make testmath (trying to test most Mathematica modes and options)

([here](install.md) you can find more installation details including required libraries)

### Usage ###

The syntax of FIESTA was changed starting with version 4.3
Now there are no global variables like NumberOfSubkernels that have to be set, everything was moved to options.

You can load FIESTA directly with
* Get["FIESTA5.m"];
or with a full path.

Look for examples in the examples folder, but probably you already know how to use it.
Just change the use of options.
For example, instead of NumberOfSubkernels = 4 use
either
* SetOptions[FIESTA, "NumberOfSubkernels" -> 4];
or add NumberOfSubkernels -> 4 to all calls of SDEvaluate and other functions.

You can set multiple options in one SetOptions or use multiple options in the SDEvaluate calls.
Options specified directly in a SDEvaluate call do not influence subsequent calls.

The list of options in use is printed with every evaluation.

For the list of versions and changes see the [changelog](changelog.md)

For usage instructions in detail see [usage](usage.md)

