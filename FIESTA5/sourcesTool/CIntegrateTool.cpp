/** @file CIntegrateTool.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  This program is used to print integrands from the database.
 *  It can be used for debugging, performance tests and in cases when something crashes and
 *  CIntegratePool prints a command to get out a problematic integrand
 */

#include <kclangc.h>
#include <getopt.h>
#include <string>
#include <map>
#include <iostream>

#include "../sourcesCommon/util.h"

/**
 * Prints help on program usage
 * @param longOptions The struct containing options of the program
 */
void ShowHelp(const option* longOptions) {
    std::map<std::string, std::string> expl;
    expl.insert(std::pair{"help", "Show this help."});
    expl.insert(std::pair{"in", "Obligatory option. Specifies input database. If name does not end with .kch, the extension is appended automatically. "
                "\nIf name ends with in.kch, automatically sets the output database with the same value but ending with out.kch in case it was not set."});
    expl.insert(std::pair{"out", "Redirects integral output to the out file. This of course could be achieved by pipe redirecting,"
                "\nhowever the advantage is that only integral output will got to the file, but other possible messages won't."});
    expl.insert(std::pair{"task", "Specify integration task for information to be obtained."
                "\nIf option is not specified prints the list of possible tasks in the database."});
    expl.insert(std::pair{"prefix", "Specify integration prefix for information to be obtained."
                "\nIf option is not specified prints the list of possible prefixes corresponding to given task."
                "\nPrefix key format in the database is n-{k, l}, where n is ep power, k is small variable power, l is small variable logarithm power."
                "\nThe space between k and l can be skipped for easier parameter entering, otherwise cover in into ""."
                "\nFor no-expansion tasks k and l are equal to zero, thus simply providing n is also allowed (tool assumes {0, 0} to be added)."
                "\nIf option is not specified prints the list of possible tasks in the database."});
    expl.insert(std::pair{"list", "List the number of functions for each of the sectors for task and prefix."
                "\nIf this option is present, the sector and following options are ignored"});
    expl.insert(std::pair{"sector", "Each prefix integration uses a number of sectors numbered from 1 to something."
                "\nThis option sets the sector number to be used to obtain the integration expression."
                "\nIf this option is missing, the number of sectors for a given task and prefix is printed."});
    expl.insert(std::pair{"function", "Each sector for a given prefix and tasks contains a number of functions."
                "\nBy default CIntegratePool joins them all as one integration job, but this can be changes by the separate_terms option of the pool."
                "\nIf the function option is present, the tool produces a single function as output."
                "\nIf the function and all options are not present, the number of functions in this sector is printed."});
    expl.insert(std::pair{"all", "Combine all entries in a sector which is the default option for CIntegratePool."
                "\nThis option is ignored if the function option is present."});
    expl.insert(std::pair{"Integrator", "Sets the integrator provided by one of the integration libraries."});
    expl.insert(std::pair{"IntegratorOption", "Sets one of the integrator parameters. \nThe syntax for <value> is <option_name>:<option_value>."});
    expl.insert(std::pair{"Math", "Provides path to the math binary for the integrators. Makes evaluation of non-predefined constants possible."});
    expl.insert(std::pair{"Statistics", "Add statistics print after integral evaluation"});
    expl.insert(std::pair{"FTest", "Instead of the integration picks the F-function from the database and puts integrators in testF mode."
                "\nIn this mode the functions are also integrated (however the result of F-integration is meaningless)"
                "\nHowever the main purpose is a test that F functions always have same sign (needed in complex mode)."
                "\nIn case they are not, an x value is printed and an error is produced."});
    expl.insert(std::pair{"CPUUsedPerWorker", "Option passed to the integration library in order to make it run <value> threads."
                "\nCurrently supported only by the Cuba library."});
    expl.insert(std::pair{"Native", "Forces integrators to use native evaluations only, no MPFR."});
    expl.insert(std::pair{"NoOptimization", "Switches off optimization in CIntegrate"});
    expl.insert(std::pair{"MPFR", "Forces integrators to use MPFR evaluations only, no native arithmetics."
                "\nThe following options are used for fine-tuning the point where CIntegrateMP switches to MPFR."});
    expl.insert(std::pair{"MPThreshold", "Defines the limit for the worst monomial in SmallX test point, so that for smaller products MPFR is turned on."
                "\nDefault value is 1E-9"});
    expl.insert(std::pair{"MPPrecision", "Redefines default precision (in bits) for calculations with MPFR."
                "\nDefault is 64, for comparisson default precision for doubles is 53."});
    expl.insert(std::pair{"SmallX", "Sets the coordinate value (all x) for the small point where the worst monom is measured, default is 0.001."});
    expl.insert(std::pair{"PrecisionShift", "Sets additional bits for mpfr precision when performing calculations in a point, default is 38."});
    expl.insert(std::pair{"MPMin", "Sets the limit for the worst monom on where to stop using defaut precision and switch to shifts. Default is 1E-48."});

    printf("Usage: ./bin/CIntegrateTool [options]\n");
    for (auto current_option = longOptions; current_option->name != nullptr; ++current_option) {
        auto expl_itr = expl.find(current_option->name);
        std::string value;
        if (expl_itr != expl.end()) {
            value = expl_itr->second;
        } else {
            value = "NO EXPLANATION YET!";
        }
        printf("\t");
        size_t spaces = 0;
        if (current_option->val > 32) {
            printf("-%c", static_cast<const char>(current_option->val));
            spaces += 2;
            if (current_option->has_arg == required_argument) {
                printf(" <value>");
                spaces += 8;
            }
            printf(", ");
            spaces += 2;
        }
        printf("--%s", current_option->name);
        spaces += 2;
        spaces += strlen(current_option->name);
        if (current_option->has_arg == required_argument) {
            printf(" <value>");
            spaces += 8;
        }
        while (spaces < 32) {
            printf(" ");
            ++spaces;
        }
        printf("\t");
        std::string info{value};
        ReplaceAll(info, "\n","\n\t");
        while (spaces) {
            ReplaceAll(info, "\n","\n ");
            --spaces;
        }
        ReplaceAll(info, "\n","\n\t");
        std::cout << info << std::endl;
    }
}

/**
 * Main function of CIntegrateTool, instrument to get integrals out of the database
 * Run it with --help to see details
 * @param argc number of arguments
 * @param argv the arguments
 * @return error code
 */
int main(int argc, char* argv[]) {

    option longOptions[] =
    {
        {"help", no_argument, nullptr, 'h'},
        {"in",   required_argument,       nullptr, 'i'},
        {"out",   required_argument,       nullptr, 'o'},
        {"task",   required_argument,       nullptr, 't'},
        {"prefix",   required_argument,       nullptr, 'p'},
        {"sector",   required_argument,       nullptr, 's'},
        {"list",   no_argument,       nullptr, 'l'},
        {"function",   required_argument,       nullptr, 'f'},
        {"all",   no_argument,       nullptr, 'a'},
        {"FTest",  no_argument, nullptr,    'F'},
        {"Statistics",  no_argument, nullptr,    'S'},
        {"Integrator",  required_argument, nullptr,    'I'}, // the following options are used purely to create the .int file with corresponding options
        {"IntegratorOption",  required_argument, nullptr,    'O'},
        {"Math",   required_argument,     nullptr, 'M'},
        {"MPFR",  no_argument, nullptr,    11},
        {"Native",  no_argument, nullptr,    19},
        {"NoOptimization",  no_argument, nullptr,    20},
        {"CPUUsedPerWorker",  required_argument, nullptr,    12},
        {"MPThreshold",  required_argument, nullptr,    2},
        {"MPPrecision",  required_argument, nullptr,    3},
        {"PrecisionShift",  required_argument, nullptr,    4},
        {"SmallX",  required_argument, nullptr,    5},
        {"MPMin",  required_argument, nullptr,    6},
        {nullptr, 0,                  nullptr,    0}
    };


    char short_options[256];
    char* pos = short_options;
    for (auto current_option = longOptions; current_option->name != nullptr; ++current_option) {
        if (current_option->val > 32) {
            *pos = current_option->val;
            ++pos;
            if (current_option->has_arg == required_argument) {
                *pos = ':';
                ++pos;
            }
        }
    }

    int c = 0;
    bool help_requested = false;
    bool in_defined = false;
    char in[128];
    bool task_defined = false;
    char task[8];
    bool prefix_defined = false;
    char prefix[32];
    bool sector_defined = false;
    char sector[8];
    bool function_defined = false;
    char function[8];
    bool all_functions = false;
    bool FTest = false;
    bool out_defined = false;
    char out[128];
    bool print_statistics = false;
    bool list_sectors = false;

    std::string integral_options = {};

    while ((c = getopt_long(argc, argv, short_options, longOptions, nullptr)) != -1) {
        std::string optionArgument = optarg ? std::string(optarg) : "";
        switch (c) {
            case 0:
                break;
            case 'h':
                ShowHelp(longOptions);
                help_requested = true;
                break;
            case 'i':
                if (optionArgument.find(".kch") == std::string::npos)
                    sprintf(in, "%s.kch", optarg);
                else
                    sprintf(in, "%s", optarg);
                in_defined = true;
                break;
            case 't':
                sprintf(task, "%s", optarg);
                task_defined = true;
                break;
            case 'o':
                sprintf(out, "%s", optarg);
                out_defined = true;
                break;
            case 'p':
                {
                bool had_bracket = false;
                char* pos_to = prefix;
                char* pos_from = optarg;
                while (*pos_from != '\0') {
                    *pos_to = *pos_from;
                    if (*pos_from == '}') had_bracket = true;
                    ++pos_to;
                    if (*pos_from == ',' && pos_from[1] != ' ') {
                        *pos_to = ' ';
                        ++pos_to;
                    }
                    ++pos_from;
                }
                *pos_to = '\0';
                if (!had_bracket) {
                    sprintf(pos_to, "-{0, 0}");
                }
                prefix_defined = true;
                break;
                }
            case 'l':
                list_sectors = true;
                break;
            case 's':
                sprintf(sector, "%s", optarg);
                sector_defined = true;
                break;
            case 'f':
                sprintf(function, "%s", optarg);
                function_defined = true;
                break;
            case 'a':
                all_functions = true;
                break;
            case 'F':
                FTest = true;
                break;
            case 'S':
                print_statistics = true;
                break;
            case 'I':
                integral_options = integral_options +
                    "SetIntegrator\n" +
                    optarg + "\n";
                break;
            case 'O': {
                const char* spos = optarg;
                while ((*spos != '\0') && (*spos != ':')) ++spos;
                if (*spos != ':') {
                    printf("Incorrect syntax after -O, parameter and value %%s:%%s requied\n");
                    return 1;
                }
                integral_options = integral_options +
                    "SetCurrentIntegratorParameter\n" +
                    std::string(optarg, spos - optarg) + "\n" +
                    std::string(spos + 1) + "\n";
                }
                break;
            case 'M':
                integral_options = integral_options +
                    "SetMath\n" +
                    optarg + "\n";
                break;
            case 11:
                integral_options = integral_options +
                    "MPFR\n";
                break;
            case 19:
                integral_options = integral_options +
                    "Native\n";
                break;
            case 20:
                integral_options = integral_options +
                    "NoOptimization\n";
                break;
            case 12:
                integral_options = integral_options +
                    "CPUCores\n" +
                    optarg + "\n";
                break;
            case 2:
                integral_options = integral_options +
                    "SetMPThreshold\n" +
                    optarg + "\n";
                break;
            case 3:
                integral_options = integral_options +
                    "SetMPPrecision\n" +
                    optarg + "\n";
                break;
            case 4:
                integral_options = integral_options +
                    "SetMPPrecisionShift\n" +
                    optarg + "\n";
                break;
            case 5:
                integral_options = integral_options +
                    "SetSmallX\n" +
                    optarg + "\n";
                break;
            case 6:
                integral_options = integral_options +
                    "SetMPMin\n" +
                    optarg + "\n";
                break;
        }
    }

    if (help_requested) return 0;

    if (!in_defined) {
        printf("Database path not defined.\n");
        return 1;
    }

    char res[256];
    KCDB* db;

    db = kcdbnew();

    snprintf(res, 256, "%s#opts=c#zcomp=gz", in);

    if (!(kcdbopen(db, res, KCOREADER | KCONOLOCK | KCONOREPAIR))) {
        printf("Database (%s) open error\n", in);
        return 1;
    }

    char entry[256];

    if (!task_defined) {
        sprintf(entry, "0-");
        char* val;
        size_t st;
        val = kcdbget(db, entry, strlen(entry), &st);
        if (!val) {
            printf("Database entry (%s) missing\n", entry);
            kcdbclose(db);
            kcdbdel(db);
            return 1;
        }
        printf("Possible tasks are: %s\n", val);
        kcfree(val);
        kcdbclose(db);
        kcdbdel(db);
        return 0;
    }

    if (!prefix_defined) {
        sprintf(entry, "%s-ForEvaluationString", task);
        char* val;
        size_t st;
        val = kcdbget(db, entry, strlen(entry), &st);
        if (!val) {
            printf("Database entry (%s) missing\n", entry);
            kcdbclose(db);
            kcdbdel(db);
            return 1;
        }
        printf("Possible prefixes for task %s are: %s\n", task, val);
        kcfree(val);
        kcdbclose(db);
        kcdbdel(db);
        return 0;
    }

    if (list_sectors || !sector_defined) {
        sprintf(entry, "%s-SCounter", task);
        char* val;
        size_t st;
        val = kcdbget(db, entry, strlen(entry), &st);
        if (!val) {
            printf("Database entry (%s) missing\n", entry);
            kcdbclose(db);
            kcdbdel(db);
            return 1;
        }
        if (!list_sectors) {
            printf("The number of sectors for task %s is: %s\n", task, val);
            kcfree(val);
        } else {
            int number_of_sectors;
            number_of_sectors = std::stoi(val);
            kcfree(val);
            printf("{");
            for (int i = 1; i <= number_of_sectors; ++i) {
                sprintf(entry, "%s-%s-%d", task, prefix, i);
                val = kcdbget(db, entry, strlen(entry), &st);
                if (!val) {
                    printf("Database entry (%s) missing\n", entry);
                    kcdbclose(db);
                    kcdbdel(db);
                    return 1;
                }
                printf("{%d, %s}", i, val);
                kcfree(val);
                if (i < number_of_sectors) {
                    printf(", ");
                }
            }
            printf("}\n");
        }
        kcdbclose(db);
        kcdbdel(db);
        return 0;
    }

    int function_start;
    int function_end;
    if (!function_defined) {
        sprintf(entry, "%s-%s-%s", task, prefix, sector);
        char* val;
        size_t st;
        val = kcdbget(db, entry, strlen(entry), &st);
        if (!val) {
            printf("Database entry (%s) missing\n", entry);
            kcdbclose(db);
            kcdbdel(db);
            return 1;
        }
        if (!all_functions) {
            printf("The number of functions for task %s prefix %s and sector %s is: %s\n", task, prefix, sector, val);
        }
        function_start = 1;
        function_end = kcatoi(val);
        kcfree(val);
        if (!all_functions) {
            kcdbclose(db);
            kcdbdel(db);
            return 0;
        }
    } else {
        function_start = function_end = kcatoi(function);
    }

    if (function_defined) {
        sprintf(entry, "%s-%s-%s-%s-N", task, prefix, sector, function);
    } else {
        sprintf(entry, "%s-%s-N", task, prefix);
    }
    char* val;
    size_t st;
    val = kcdbget(db, entry, strlen(entry), &st);
    if (!val) {
        printf("Database entry (%s) missing\n", entry);
        kcdbclose(db);
        kcdbdel(db);
        return 1;
    }
    //printf("The maximal dimension for task %s and prefix %s is: %s\n", task, prefix, val);
    int n = kcatoi(val);
    kcfree(val);

    if (out_defined) {
        if (!freopen(out, "w", stdout)) {
            printf("Output file could not be opened");
            kcdbclose(db);
            kcdbdel(db);
            return 1;
        }
    }

    printf("%s", integral_options.c_str());

    printf("Integrate\n");

    if (function_defined)
        printf("%d;\n0;\n", n);
    else
        printf("%d;\n%d;\n", n, function_end - function_start + 1);

    for (int i = function_start; i <= function_end; i++) {
        // F can be added
        snprintf(res, 256, "%s-%s-%s-%d%c", task, prefix, sector, i, FTest ? 'F' : '\0');
        val = kcdbget(db, res, strlen(res), &st);
        if (!val) {
            fprintf(stderr, "Integral entry (%s) missing\n", res);
            return 1;
        }
        printf("%s\n", val);
        kcfree(val);
    }

    if (!function_defined) {
        for (int i = function_start; i <= function_end; i++) {
            printf("f[%d]+", i);
        }
        printf("0;\n");
    }

    printf("|\n");

    if (print_statistics) {
        printf("Statistics\n");
    }

    printf("Exit\n");

    kcdbclose(db);
    kcdbdel(db);

    return 0;
}
