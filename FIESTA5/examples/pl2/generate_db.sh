source paths.inc 2>/dev/null
if ([ ! -d "examples/pl2/temp/" ]) then
    mkdir examples/pl2/temp;
fi;
if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ] && [ "$#" -ne 3 ]) then
    echo "Argument with a master integral number required";
else
    if ([ "$#" -ne 1 ]) then
        cores="$2";links=$2;
    else
        cores=0;links=1;
    fi;
    if ([ "$#" -ne 1 ] && [ "$#" -ne 2 ]) then
        strategy="$3";
    else
        strategy="S";
    fi;
    { echo "Get[\"FIESTA5.m\"]; SetOptions[FIESTA, NumberOfSubkernels -> $cores, NumberOfLinks -> $links, Strategy -> \"$strategy\"]; MasterIndex = $1; Print[\"Generating for master integral \" <> ToString[MasterIndex]];"; cat examples/pl2/generate_db.m; } | ${MATH}
fi;

