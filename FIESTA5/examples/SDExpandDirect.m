Get["examples/include.m"];
result = SDExpandDirect[
    {1, x[1] x[2] x[3] + x[1] x[2] x[4] + x[1] x[3] x[4] + x[1] x[2] x[5], x[1] x[3] + x[2] x[3] + x[1] x[4] + x[2] x[4] + t (x[3] x[4] + x[1] x[5] + x[2] x[5] + x[3] x[5])},
    {1, -1 - 2 ep, -1 + 3 ep},
    0,
    0,
    {{1, 2, 3, 4, 5}},
    MathematicaBinary -> Automatic,
    Strategy -> "S",
    OnlyPrepare -> True,
    DataPath -> Default
];
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
