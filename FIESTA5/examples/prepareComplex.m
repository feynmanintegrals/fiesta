SetOptions[$Output,PageWidth->Infinity];
ComplexMode = True;
NumberOfLinks = 4;
NumberOfSubkernels = 4;
OnlyPrepare = True;
DataPath = "temp/dbcomplex";
Get["FIESTA5.m"];
SDEvaluate[
 UF[{k, l}, {mm - k^2, -(k + p1)^2, -(k + p1 + p2)^2 +
    mm, -(k - l + p1 + p2 + p3)^2 + mm, -(k - l)^2,
   mm - l^2, -(l - p3)^2}, {p1^2 -> mm, p2^2 -> mm, p3^2 -> mm,
   p1 p2 -> 1/2 (-2 mm - S), p1 p3 -> 1/2 (-2 mm - T),
   p2 p3 -> 1/2 (2 mm + S + T), mm -> 1, S -> 0.3, T -> 0.7}], {1, 1, 1, 1, 1, 1, 1}, -1];




