Get["examples/include.m"];
result = SDExpandAsy[
    {x[1] + x[2] + x[3] + x[4], x[1] x[3] + t x[2] x[4], 1},
    {1 + la, 1, 1 - la, 1},
    1,
    0,
    RegVar -> la,
    ExpandVar -> t
];
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
