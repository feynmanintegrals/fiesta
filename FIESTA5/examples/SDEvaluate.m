Get["examples/include.m"];
result1=SDEvaluate[{x[1] + x[2] + x[3] + x[4], x[1] x[3] + x[2] x[4], 1}, {0, 1, 1, 1}, 1, NumberOfSubkernels->3, SeparateTerms->True];
result2=SDEvaluate[{x[1] + x[2] + x[3] + x[4], x[1] x[3] + x[2] x[4], 1} /. x->y, {0, 1, 1, 1}, 1, NumberOfSubkernels->4, XVar -> y];
result3=SDEvaluate[{x[1] + x[2] + x[3] + x[4], x[1] x[3] + x[2] x[4], 1}, {0, 1, 1, 1}, 1, EpVar -> epsilon];
result = {result1, result2, result2};
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
