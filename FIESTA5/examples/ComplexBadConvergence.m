SetOptions[$Output,PageWidth->Infinity];
Get["FIESTA5.m"];
SetOptions[FIESTA,
    "SectorSymmetries" -> True,
    "NumberOfSubkernels" -> 4,
    "NumberOfLinks" -> 4,
    "ContourShiftShape" -> 4,
    "Integrator" -> "quasiMonteCarlo",
    "DataPath" -> Default,
    "ComplexMode" -> True,
    "OnlyPrepare" -> True,
    "SeparateTerms" -> True,
    "Precision" -> 16,
    "IntegratorOptions" -> {{"maxeval", "5000000"}}
];

Propagators = {-k1^2 + mm, -(k1 + p1 + p2)^2 +
    mm, -k2^2, -(k2 + p1 + p2)^2,
   -(k1 + p1)^2, -(k1 - k2)^2 + mm, -(k2 - p3)^2 +
    mm, -(k2 + p1)^2, -(k1 - p3)^2};
Replacements = {p1^2 -> mm, p2^2 -> mm, p3^2 -> mm, p1 p2 -> 1/2 (-2 mm - S),
p1 p3 -> 1/2 (-2 mm - T),
p2 p3 -> 1/2 (2 mm + S + T), mm -> 1, S -> -3/10, T -> 7/10};
indices = {1, 1, 1, 1, 1, 1, 1, 0, 0};
zeros = Position[indices, 0];
result = SDEvaluate[
   UF[{k1, k2}, Delete[Propagators, zeros], Replacements],
   Delete[indices, zeros], 0];
Print["Known answer is ", InputForm[(13.09273756 - 18.34473452 I) - 0.7877508045/ep^2 - ( 1.729249721 + 4.949584281 I)/ep]];
