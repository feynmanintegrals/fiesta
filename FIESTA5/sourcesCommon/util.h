/** @file util.h
 *
 *  This file is a part of the FIESTA package
 *
 *  It contains definitions of functions used in more that one part of the project
 */

#pragma once

#include <string>
#include <vector>

/**
 * A macro resulting in crash and program stop
 * @param message Message to print
 */
#define FIESTA_FAIL(message) \
    do { \
        auto msg1 = std::string(message); \
        fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, msg1.c_str()); \
        abort(); \
    } while (false)

/**
 * A macro checking a condition and failing if it is not true
 * @param cond Condition to check
 * @param message Message to print
 */
#define FIESTA_VERIFY(cond, message) \
    do { \
        if (!(cond)) { \
            auto msg = std::string("FIESTA_VERIFY(" #cond ") failed: ") + message; \
            FIESTA_FAIL(msg); \
        } \
    } while (false)

/**
 * String generation with format and arguments
 * @param s format string as in printf
 * @param args arguments
 * @return The generated string
 */
template <class... Ts>
std::string Format(const char* s, Ts&& ...args) {
    std::vector<char> res(256);
    auto total = snprintf(res.data(), res.size(), s, args...);
    FIESTA_VERIFY(total >= 0, "snprintf() failed");
    if (total >= static_cast<int>(res.size())) {
        res.resize(total + 1);
        auto newTotal = snprintf(res.data(), res.size(), s, args...);
        FIESTA_VERIFY(total == newTotal, "snprintf() returned different number on second call");
    }
    return std::string(res.data(), total);
}

/**
 * String generation with format and arguments for other types
 * @param s format string as in printf
 * @param args arguments
 * @return The generated string
 */
template <class... Ts>
std::string Format(const std::string& s, Ts&& ...args) {
    return Format(s.c_str(), args...);
}

/**
 * Replaces all occurencies in a string
 * @param str The string being changed
 * @param from The substring being replaced
 * @param to The replacement
 */
void ReplaceAll(std::string& str, const std::string& from, const std::string& to);

/**
 * Class executing given callback in destructor.
 */
template <typename TFun>
class FinallyGuard {
public:
    /**
     * Construct the guard
     * @param f callback to execute on destruction
     */
    FinallyGuard(TFun f)
        : f(f)
    { }

    /**
     * Desctruct the guard and execute the callback
     */
    ~FinallyGuard() {
        f();
    }

private:
    TFun f;  /**< Callback to execute on destruction */
};

/**
 * Creates FinallyGuard.
 * @param f callback to execute on destruction
 * @return the guard
 */
template <typename TFun>
[[nodiscard]] FinallyGuard<TFun> Finally(TFun f) {
    return FinallyGuard<TFun>(f);
}
