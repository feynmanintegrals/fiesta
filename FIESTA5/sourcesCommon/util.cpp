/** @file util.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  It contains implementations of functions used in more that one part of the project
 */

#include "util.h"

void ReplaceAll(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
}

