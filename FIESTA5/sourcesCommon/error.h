/** @file error.h
 *
 *  This file is a part of the FIESTA package
 *
 *  It contains definitions of classes and functions corresponding to exceptions.
 */

#pragma once

#include <exception>
#include <string>

/**
 * Common class for all our exceptions.
 */
class FiestaException
    : public std::exception
{
public:
    /**
     * Construct exception with given message
     * @param message
     */
    explicit FiestaException(const std::string& message)
        : message(message)
    { }

    /**
     * Get description of the exception
     * @return description
     */
    virtual const char* what() const noexcept override {
        return message.c_str();
    }

private:
    std::string message;  /**< Message */
};
