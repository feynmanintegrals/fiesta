/** @file integrators.h
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains definition of integrators used to seed sampling points
 */

#pragma once

#include "runtime.h"
#include "scanner.h"

#include <string>
#include <memory>
#include <variant>
#include <cuba.h>

#ifdef ENABLE_TT
#include <tt/integrate/config.h>
#include <tt/integrate/integrate_interface.h>
#endif

#ifdef ENABLE_QMC
#include "../extra/qmc/src/qmc.hpp"
#endif

/**
 * The structure containing the answer and the error
 */
class IntegrationResult {
    public:
        double resultReal;  /**<Real part of the integral*/
        double errorReal;  /**<Real part of the error estimate*/
        double resultImaginary;  /**<Imaginary part of the integral*/
        double errorImaginary;  /**<Imaginary part of the error estimate*/
};

/**
 * Integrator evaluating function that seeds points and requests runExpr for integrand values
 */
typedef int (*IntegrationFunction)(IntegrationResult *result);

/**
 * Class storing a parameter of a specific integrator
 */
class IntegratorParameter {
    public:
    const std::string name; /**< Parameter name*/
    const std::string fmt; /**< Parameter type like in printf*/
    std::variant<int*, long long int*, double*, char*> addr; /**< Address of the parameter value*/

    /**
     * Constructor bases on a pointer to int
     * @param name parameter name
     * @param fmt parameter format
     * @param p pointer to an int
     */
    IntegratorParameter(const std::string& name, const std::string& fmt, int* p) : name(name), fmt(fmt) {
       addr = p;
    }

    /**
     * Constructor bases on a pointer to long long int
     * @param name parameter name
     * @param fmt parameter format
     * @param p pointer to a long long int
     */
    IntegratorParameter(const std::string& name, const std::string& fmt, long long int* p) : name(name), fmt(fmt) {
       addr = p;
    }

    /**
     * Constructor bases on a pointer to double
     * @param name parameter name
     * @param fmt parameter format
     * @param p pointer to a double
     */
    IntegratorParameter(const std::string& name, const std::string& fmt, double* p) : name(name), fmt(fmt) {
       addr = p;
    }

    /**
     * Constructor bases on a pointer to c-based string
     * @param name parameter name
     * @param fmt parameter format
     * @param p pointer, char*
     */
    IntegratorParameter(const std::string& name, const std::string& fmt, char* p) : name(name), fmt(fmt) {
       addr = p;
    }
};

/**
 * Class defining static members - list of integrators and functions to work with them
 */
class Integration {
    private:
        /**
         * Internal map storing correspondence of integrator names and integrators
         */
        inline static std::unordered_map<std::string, std::pair<IntegrationFunction, std::vector<IntegratorParameter>>> integrators_ = {};
        /**
         * Indication whether integrators were initialized
         */
        inline static bool init = false;
    public:
        /**
         * The name of the currently used integrator
         */
        inline static std::string currentIntegrator = {};

        /**
         * Integration Runtime. Set up in CIntegrate.cpp, used by integrators.
         */
        inline static std::unique_ptr<Runtime> globalRuntime = {};

        /**
         * Function registering all integrators and setting default integrator
         * @return void
         */
        static void InitIntegrators();

        /**
         * Function registering an integrator
         * @param theIntegrator integrator being registered
         * @param name integrator name
         * @param params vector of parameters this integrator accept
         * @return void
         */
        static void RegisterIntegrator(IntegrationFunction theIntegrator, const std::string& name, const std::vector<IntegratorParameter>& params);

        /**
         * Sets currently used integrator
         * @param integratorName the name of the integrator
         * @return void
         */
        static void SetIntegrator(const std::string& integratorName);

        /**
         * Sets an integrator parameter
         * @param integratorName the name of the integrator
         * @param name name of the parameter
         * @param val value of the parameter
         * @return void
         */
        static void SetParameter(const std::string& integratorName, const std::string& name, const std::string& val);

        /**
         * Returns all parameters of an integrator as a string
         * @param integratorName the name of the integrator
         * @return a string of integrater prameters incurly brackets, for example {{"maxeval", "50000"}, ...}. Such a string can be interpreted from Mathematics.
         */
        static std::string GetAllParameters(const std::string& integratorName);

        /**
         * Calls the integrator with a given name
         * @param integratorName the name of the integrator
         * @return struct with results and error estimates
         */
        static IntegrationResult RunIntegrator(const std::string& integratorName);
};
