/** @file Runtime.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  Here the Runtime is implemented based on a scan class.
 *  After building triads we are ready to run the runExpr function evaluating the integrand in a batch of points.
 */

#include "runtime.h"

#include <math.h>

#if defined(ENABLE_AVX) || defined(DOXYGEN_DOCUMENTATION)
#include <immintrin.h>
#ifndef ENABLE_ICC
#include <vectorclass.h>
#include <vectormath_exp.h>
#endif
#endif

#include "scanner.h"
#include "../sourcesCommon/util.h"

/**
 * Macro settinf that precision in MP mode should be controlled
 * Might be switched off for performance, but it needs to be measured first
 */
#define DEBUG_PREC 1

#ifndef COMPLEX

/**
 * Addition in native mode
 */
#define NP_PLUS(a, b, c, shift, sim) a[shift] = b[shift] + c[shift]
/**
 * Multiplication in native mode
 */
#define NP_MUL(a, b, c, shift, sim) a[shift] = b[shift] * c[shift]
/**
 * Substraction in native mode
 */
#define NP_MINUS(a, b, c, shift, sim) a[shift] = b[shift] - c[shift]
/**
 * Division in native mode
 */
#define NP_DIV(a, b, c, shift, sim) a[shift] = b[shift] / c[shift]
/**
 * Integer power in native mode
 */
#define NP_IPOW(a, b, deg, shift, sim)    \
    {                                     \
        double x = b[shift];               \
        double z;                          \
        int y = deg;                      \
        double u;                          \
        if (y == 2)                       \
            u = x * x;                    \
        else {                            \
            if ((y & 1) != 0)             \
                u = x;                    \
            else                          \
                u = 1.0L;                 \
            z = x;                        \
            y >>= 1;                      \
            while (y) {                   \
                z = z * z;                \
                if ((y & 1) != 0) u *= z; \
                y >>= 1;                  \
            }                             \
        }                                 \
        a[shift] = u;                     \
    }
/**
 * Power 2 in native mode
 */
#define NP_POW2(a, b, shift, sim) (a[shift]) = (b[shift]) * (b[shift])
/**
 * Power 3 in native mode
 */
#define NP_POW3(a, b, shift, sim) (a[shift]) = (b[shift]) * (b[shift]) * (b[shift])
/**
 * Power 4 in native mode
 */
#define NP_POW4(a, b, shift, sim)         \
    {                                     \
        double temp;                       \
        (temp) = (b[shift]) * (b[shift]); \
        (a[shift]) = (temp) * (temp);     \
    }
/**
 * Power 5 in native mode
 */
#define NP_POW5(a, b, shift, sim)                  \
    {                                              \
        double temp;                                \
        (temp) = (b[shift]) * (b[shift]);          \
        (a[shift]) = (temp) * (temp) * (b[shift]); \
    }
/**
 * Power 6 in native mode
 */
#define NP_POW6(a, b, shift, sim)              \
    {                                          \
        double temp;                            \
        (temp) = (b[shift]) * (b[shift]);      \
        (a[shift]) = (temp) * (temp) * (temp); \
    }
/**
 * Copy operation in native mode
 */
#define NP_CPY(a, b, shift, sim) (a[shift]) = (b[shift])
/**
 * Negative number in native mode
 */
#define NP_NEG(a, b, shift, sim) (a[shift]) = -(b[shift])
/**
 * Inverse number in native mode
 */
#define NP_INV(a, b, shift, sim) (a[shift]) = 1.0 / (b[shift])
/**
 * Logarithm in native mode
 */
#define NP_LOG(a, b, shift, sim)        \
    if ((b[shift]) <= 1.0E-100)         \
        (a[shift]) = -230.258509299405; \
    else                                \
        (a[shift]) = log(double(b[shift]));
/**
 * Abritary power in native mode
 */
#define NP_POW(a, b, c, shift, sim) (a[shift]) = pow((b[shift]), (c[shift]))

#else

#define NP_PLUS(a, b, c, shift, sim) \
    a[shift] = b[shift] + c[shift];  \
    a[shift + sim] = b[shift + sim] + c[shift + sim];
#define NP_MINUS(a, b, c, shift, sim) \
    a[shift] = b[shift] - c[shift];   \
    a[shift + sim] = b[shift + sim] - c[shift + sim];
#define NP_MUL(a, b, c, shift, sim)                                       \
    a[shift] = (b[shift] * c[shift]) - (b[shift + sim] * c[shift + sim]); \
    a[shift + sim] = (b[shift + sim] * c[shift]) + (b[shift] * c[shift + sim]);
#define NP_DIV(a, b, c, shift, sim)                                                \
    double temp = (c[shift] * c[shift]) + (c[shift + sim] * c[shift + sim]);        \
    a[shift] = ((b[shift] * c[shift]) + (b[shift + sim] * c[shift + sim])) / temp; \
    a[shift + sim] = ((b[shift + sim] * c[shift]) - (b[shift] * c[shift + sim])) / temp;

#define NP_IPOW(a, b, deg, shift, sim)        \
    int y = deg;                              \
    double bre = b[shift];                     \
    double bim = b[shift + sim];               \
    double temp;                               \
    if (y == 2) {                             \
        a[shift] = (bre * bre) - (bim * bim); \
        a[shift + sim] = 2 * (bre * bim);     \
    } else {                                  \
        double are, aim;                       \
        if ((y & 1) != 0) {                   \
            are = bre;                        \
            aim = bim;                        \
        } else {                              \
            are = 1.0L;                       \
            aim = 0.0L;                       \
        }                                     \
        y >>= 1;                              \
        while (y) {                           \
            temp = bre * bre - bim * bim;     \
            bim = 2 * bre * bim;              \
            bre = temp;                       \
            if ((y & 1) != 0) {               \
                temp = are * bre - aim * bim; \
                aim = are * bim + aim * bre;  \
                are = temp;                   \
            }                                 \
            y >>= 1;                          \
        }                                     \
        a[shift] = are;                       \
        a[shift + sim] = aim;                 \
    }

#define NP_POW2(a, b, shift, sim)                                         \
    a[shift] = (b[shift] * b[shift]) - (b[shift + sim] * b[shift + sim]); \
    a[shift + sim] = 2 * (b[shift + sim] * b[shift]);
#define NP_POW3(a, b, shift, sim)                                                               \
    a[shift] = b[shift] * b[shift] * b[shift] - 3 * b[shift] * b[shift + sim] * b[shift + sim]; \
    a[shift + sim] = 3 * b[shift + sim] * b[shift] * b[shift] - b[shift + sim] * b[shift + sim] * b[shift + sim];
#define NP_POW4(a, b, shift, sim)                                              \
    {                                                                          \
        double tre = (b[shift] * b[shift]) - (b[shift + sim] * b[shift + sim]); \
        double tim = 2 * (b[shift + sim] * b[shift]);                           \
        a[shift] = (tre * tre) - (tim * tim);                                  \
        a[shift + sim] = 2 * (tre * tim);                                      \
    }
#define NP_POW5(a, b, shift, sim) \
    { NP_IPOW(a, b, 5, shift, sim) }

#define NP_POW6(a, b, shift, sim)                                              \
    {                                                                          \
        double tre = (b[shift] * b[shift]) - (b[shift + sim] * b[shift + sim]); \
        double tim = 2 * (b[shift + sim] * b[shift]);                           \
        a[shift] = (tre * tre * tre) - (3 * tre * tim * tim);                  \
        a[shift + sim] = (3 * tre * tre * tim) - (tim * tim * tim);            \
    }
#define NP_CPY(a, b, shift, sim) \
    (a[shift]) = (b[shift]);     \
    (a[shift + sim]) = (b[shift + sim])
#define NP_NEG(a, b, shift, sim) \
    (a[shift]) = -(b[shift]);    \
    (a[shift + sim]) = -(b[shift + sim])
#define NP_INV(a, b, shift, sim)                                            \
    {                                                                       \
        double temp = b[shift] * b[shift] + b[shift + sim] * b[shift + sim]; \
        a[shift] = b[shift] / temp;                                         \
        a[shift + sim] = -b[shift + sim] / temp;                            \
    }

#define NP_LOG(a, b, shift, sim)                             \
    double bre = b[shift];                                    \
    double bim = b[shift + sim];                              \
    if ((bre <= 1.0E-100) && (bre >= 0) && (bim == 0.0)) {   \
        a[shift] = -230.258509299405;                        \
        a[shift + sim] = 0;                                  \
    } else if ((bre >= -1.0E-100) && (bre <= 0) && (bim == 0.0)) {   \
        a[shift] = -230.258509299405;                        \
        a[shift + sim] = -M_PI;                                  \
    } else if ((bim == 0.0) && (bre > 0)) {                  \
        a[shift] = log(bre);                                 \
        a[shift + sim] = 0;                                  \
    } else if ((bim == 0.0) && (bre < 0)) {                  \
        a[shift] = log(-bre);                                \
        a[shift + sim] = -M_PI;                              \
    } else {                                                 \
        double temp = bre * bre + bim * bim;                  \
        a[shift] = log(temp) / 2;                            \
        a[shift + sim] = atan2(bim, bre);             \
    }

#define NP_POW(a, b, c, shift, sim)                                             \
    {                                                                           \
        double bre = b[shift];                                                  \
        double bim = b[shift + sim];                                            \
        FIESTA_VERIFY(((c[shift + sim]) == 0.), "complex power not supported"); \
        double power = c[shift];                                                \
        if (bim == 0.) {                                                        \
            FIESTA_VERIFY((bre >= 0.), "bad!!!");    \
            (a[shift]) = pow(bre, power);                                       \
            (a[shift + sim]) = 0.;                                              \
        } else {                                                                \
            double norm = pow(bre*bre + bim*bim, power/2);                      \
            double angle = atan2(bim, bre);                                     \
            angle *= power;                                                     \
            a[shift] = norm * cos(angle);                                       \
            a[shift + sim] = norm * sin(angle);                                 \
        }                                                                       \
    }

#endif

#ifndef COMPLEX

#ifdef DEBUG_PREC
/**
 * Check of precision for one number in MP mode
 */
#define CHC1(a) \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision);

/**
 * Check of precision for two numbers in MP mode
 */
#define CHC2(a, b)                                                                                                  \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision); \
    if (mpfr_get_prec((b).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision);

/**
 * Check of precision for three numbers in MP mode
 */
#define CHC3(a, b, c)                                                                                               \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision); \
    if (mpfr_get_prec((b).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((b).re), MPPrecision); \
    if (mpfr_get_prec((c).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((c).re), MPPrecision);
#else
#define CHC1(a)
#define CHC2(a, b)
#define CHC3(a, b, c)
#endif

/**
 * Copy operation in MP mode
 */
#define CPY(a, b) \
    { CHC2(a, b) mpfr_set((a).re, (b).re, GMP_RNDN); }
/**
 * Logarithm in MP mode
 */
#define LOG(a, b)                                                                                 \
    {                                                                                             \
        CHC1(a) if (mpfr_cmp_d((b).re, 1.0E-100) <= 0) mpfr_set_d((a).re, -230.258509299405, GMP_RNDN); \
        else mpfr_log((a).re, (b).re, GMP_RNDN);                                                        \
    }
/**
 * Negative to the number in MP mode
 */
#define NEG(a, b) \
    { CHC2(a, b) mpfr_neg((a).re, (b).re, GMP_RNDN); }
/**
 * Inverse number in MP mode
 */
#define INV(a, b) \
    { CHC2(a, b) mpfr_div((a).re, theScan->MPConstants[1].re, (b).re, GMP_RNDN); }
/**
 * Abritrary power in MP mode
 */
#define POW(a, b, c) \
    { CHC3(a, b, c) mpfr_pow((a).re, (b).re, (c).re, GMP_RNDN); }
/**
 * Integer power in MP mode
 */
#define IPOW(a, b, c) \
    { CHC2(a, b) mpfr_pow_si((a).re, (b).re, (c), GMP_RNDN); }
/**
 * Multiplication in MP mode
 */
#define MUL(a, b, c) \
    { CHC3(a, b, c) mpfr_mul((a).re, (b).re, (c).re, GMP_RNDN); }
/**
 * Power 3 in MP mode
 */
#define POW3(a, b) \
    { CHC1(a) mpfr_pow_ui((a).re, (b).re, 3, GMP_RNDN); }
/**
 * Power 4 in MP mode
 */
#define POW4(r, b) \
    { CHC1(r) mpfr_pow_ui((r).re, (b).re, 4, GMP_RNDN); }
/**
 * Power 5 in MP mode
 */
#define POW5(r, b) \
    { CHC1(r) mpfr_pow_ui((r).re, (b).re, 5, GMP_RNDN); }
/**
 * Power 6 in MP mode
 */
#define POW6(r, b) \
    { CHC1(r) mpfr_pow_ui((r).re, (b).re, 6, GMP_RNDN); }
/**
 * Substraction in MP mode
 */
#define MINUS(a, b, c) \
    { CHC3(a, b, c) mpfr_sub((a).re, (b).re, (c).re, GMP_RNDN); }
/**
 * Division in MP mode
 */
#define DIV(a, b, c) \
    { CHC3(a, b, c) mpfr_div((a).re, (b).re, (c).re, GMP_RNDN); }
/**
 * Addition in MP mode
 */
#define PLUS(a, b, c) \
    { CHC3(a, b, c) mpfr_add((a).re, (b).re, (c).re, GMP_RNDN); }

#else

#ifdef DEBUG_PREC
#define CHC1(a) \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision);

#define CHC2(a, b)                                                                                                            \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision); \
if (mpfr_get_prec((b).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((b).re), MPPrecision);

#define CHC3(a, b, c)                                                                                                         \
    if (mpfr_get_prec((a).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((a).re), MPPrecision); \
    if (mpfr_get_prec((b).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((b).re), MPPrecision); \
    if (mpfr_get_prec((c).re) < MPPrecision) fprintf(stderr, "%ld<%d\n", mpfr_get_prec((c).re), MPPrecision);
#else
#define CHC1(a)
#define CHC2(a, b)
#define CHC3(a, b, c)
#endif

#define CPY(a, b)                                      \
    {                                                  \
        CHC2(a, b) mpfr_set((a).re, (b).re, GMP_RNDN); \
        mpfr_set((a).im, (b).im, GMP_RNDN);            \
    }
#define LOG(a, b)                                                                                                      \
    {                                                                                                                  \
        if ((mpfr_cmp_d((b).re, 1.0E-100) <= 0) && (mpfr_cmp_d((b).re, 0.0) >= 0) && (mpfr_cmp_d((b).im, 0.0) == 0)) { \
            mpfr_set_d((a).re, -230.258509299405, GMP_RNDN);                                                           \
            mpfr_set_d((a).im, 0.0, GMP_RNDN);                                                                         \
        } else if ((mpfr_cmp_d((b).re, 0.0) >= 0) && (mpfr_cmp_d((b).im, 0.0) == 0)) {                                 \
            mpfr_log((a).re, (b).re, GMP_RNDN);                                                                        \
            mpfr_set_d((a).im, 0.0, GMP_RNDN);                                                                         \
        } else if ((mpfr_cmp_d((b).re, -1.0E-100) >= 0) && (mpfr_cmp_d((b).re, 0.0) <= 0) && (mpfr_cmp_d((b).im, 0.0) == 0)) { \
            mpfr_set_d((a).re, -230.258509299405, GMP_RNDN);                                                           \
            mpfr_neg((a).im, pi, GMP_RNDN);                                                                            \
        } else if ((mpfr_cmp_d((b).re, 0.0) < 0) && (mpfr_cmp_d((b).im, 0.0) == 0)) {                                  \
            mpfr_neg(temp1, (b).re, GMP_RNDN);                                                                         \
            mpfr_log((a).re, temp1, GMP_RNDN);                                                                         \
            mpfr_neg((a).im, pi, GMP_RNDN);                                                                            \
        } else {                                                                                                       \
            mpfr_mul(temp1, (b).re, (b).re, GMP_RNDN);                                                                 \
            mpfr_mul(temp2, (b).im, (b).im, GMP_RNDN);                                                                 \
            mpfr_add(denom, temp1, temp2, GMP_RNDN);                                                                   \
            mpfr_log(temp1, denom, GMP_RNDN);                                                                          \
            mpfr_set_d(temp2, 2.0, GMP_RNDN);                                                                          \
            mpfr_div((a).re, temp1, temp2, GMP_RNDN);                                                                  \
            mpfr_atan2((a).im, (b).im, (b).re, GMP_RNDN);                                                              \
        }                                                                                                              \
    }
#define NEG(a, b)                                      \
    {                                                  \
        CHC2(a, b) mpfr_neg((a).re, (b).re, GMP_RNDN); \
        mpfr_neg((a).im, (b).im, GMP_RNDN);            \
    }
#define INV(a, b)                                  \
    {                                              \
        CHC2(a, b)                                 \
        mpfr_mul(temp1, (b).re, (b).re, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (b).im, GMP_RNDN); \
        mpfr_add(denom, temp1, temp2, GMP_RNDN);   \
        mpfr_div((a).re, (b).re, denom, GMP_RNDN); \
        mpfr_neg(temp1, (b).im, GMP_RNDN);         \
        mpfr_div((a).im, temp1, denom, GMP_RNDN);  \
    }
#define POW(a, b, c)                              \
    {                                             \
        CHC3(a, b, c)                                                                \
        FIESTA_VERIFY(mpfr_cmp_d((c).im, 0.0) == 0, "Complex power not supported in mpfr"); \
        if (mpfr_cmp_d((b).im, 0.0) == 0) {                                                 \
            mpfr_pow((a).re, (b).re, (c).re, GMP_RNDN);                                      \
            mpfr_set_d((a).im, 0.0, GMP_RNDN);                                               \
        } else {                                                                             \
            mpfr_mul(temp1, (b).re, (b).re, GMP_RNDN);                                       \
            mpfr_mul(temp2, (b).im, (b).im, GMP_RNDN);                                       \
            mpfr_add(temp1, temp1, temp2, GMP_RNDN);                                         \
            mpfr_div_si(temp2, (c).re, 2, GMP_RNDN);                                         \
            mpfr_pow(temp1, temp1, temp2, GMP_RNDN);                                         \
            mpfr_set((a).re, temp1, GMP_RNDN);                                               \
            mpfr_set((a).im, temp1, GMP_RNDN);                                               \
            mpfr_atan2(temp2, (b).im, (b).re, GMP_RNDN);                                     \
            mpfr_mul(temp2, temp2, (c).re, GMP_RNDN);                                        \
            mpfr_sin(temp1, temp2, GMP_RNDN);                                                \
            mpfr_cos(temp2, temp2, GMP_RNDN);                                                \
            mpfr_mul((a).re, (a).re, temp2, GMP_RNDN);                                       \
            mpfr_mul((a).im, (a).im, temp1, GMP_RNDN);                                       \
        }                                                                                    \
    }
#define POW2(a, b)                                 \
    {                                              \
        CHC2(a, b)                                 \
        mpfr_mul(temp1, (b).re, (b).re, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (b).im, GMP_RNDN); \
        mpfr_sub((a).re, temp1, temp2, GMP_RNDN);  \
        mpfr_mul(temp1, (b).re, (b).im, GMP_RNDN); \
        mpfr_add((a).im, temp1, temp1, GMP_RNDN);  \
    }
#define IPOW(a, b, c)                         \
    {                                         \
        CHC2(a, b)                            \
        int y = c;                            \
        if (y == 2) POW2(u, b) else {         \
                if ((y & 1) != 0)             \
                    CPY(u, b)                 \
                else {                        \
                    double cc = 1.0L;          \
                    mpfr_set_d(u.re, cc, GMP_RNDN);\
                    mpfr_set_d(u.im, 0., GMP_RNDN);\
                }                             \
                CPY(z, b);                    \
                y >>= 1;                      \
                while (y) {                   \
                    POW2(v, z);               \
                    CPY(z, v);                \
                    if ((y & 1) != 0) {       \
                        MUL(v, u, z);         \
                        CPY(u, v);            \
                    }                         \
                    y >>= 1;                  \
                }                             \
            }                                 \
        CPY(a, u);                            \
    }
#define MUL(a, b, c)                               \
    {                                              \
        CHC3(a, b, c)                              \
        mpfr_mul(temp1, (b).re, (c).re, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (c).im, GMP_RNDN); \
        mpfr_sub((a).re, temp1, temp2, GMP_RNDN);  \
        mpfr_mul(temp1, (b).re, (c).im, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (c).re, GMP_RNDN); \
        mpfr_add((a).im, temp1, temp2, GMP_RNDN);  \
    }
#define POW3(a, b)                    \
    {                                 \
        CHC1(a)                       \
        MUL(temp, b, b);              \
        MUL(a, temp, b);              \
    }
#define POW4(a, b)                    \
    {                                 \
        CHC1(a)                       \
        MUL(temp, b, b);              \
        MUL(a, temp, temp);           \
    }
#define POW5(a, b)                    \
    {                                 \
        CHC1(a)                       \
        MUL(a, b, b);                 \
        MUL(temp, a, a);              \
        MUL(a, temp, b);              \
    }
#define POW6(a, b)                    \
    {                                 \
        CHC1(a)                       \
        MUL(a, b, b);                 \
        MUL(temp, a, b);              \
        MUL(a, temp, temp);           \
    }
#define MINUS(a, b, c)                              \
    {                                               \
        CHC3(a, b, c)                               \
        mpfr_sub((a).re, (b).re, (c).re, GMP_RNDN); \
        mpfr_sub((a).im, (b).im, (c).im, GMP_RNDN); \
    }
#define DIV(a, b, c)                               \
    {                                              \
        CHC3(a, b, c)                              \
        mpfr_mul(temp1, (c).re, (c).re, GMP_RNDN); \
        mpfr_mul(temp2, (c).im, (c).im, GMP_RNDN); \
        mpfr_add(denom, temp1, temp2, GMP_RNDN);   \
        mpfr_mul(temp1, (b).re, (c).re, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (c).im, GMP_RNDN); \
        mpfr_add(temp3, temp1, temp2, GMP_RNDN);   \
        mpfr_div((a).re, temp3, denom, GMP_RNDN);  \
        mpfr_mul(temp1, (b).re, (c).im, GMP_RNDN); \
        mpfr_mul(temp2, (b).im, (c).re, GMP_RNDN); \
        mpfr_sub(temp3, temp2, temp1, GMP_RNDN);   \
        mpfr_div((a).im, temp3, denom, GMP_RNDN);  \
    }
#define PLUS(a, b, c)                               \
    {                                               \
        CHC3(a, b, c)                               \
        mpfr_add((a).re, (b).re, (c).re, GMP_RNDN); \
        mpfr_add((a).im, (b).im, (c).im, GMP_RNDN); \
    }

#endif

/**
 * Definition of cycle start in native mode (done in portions of 4 to be able to switch on AVX)
 */
#define BUNCHSTART for (run = 0; run != local_bunch; run += 4) {
/**
 * Definition of cycle end in native mode (done in portions of 4 to be able to switch on AVX)
 */
#define BUNCHEND }

void Runtime::RunlineMP(double *res) {
    Operation *rto = runtimeOperations.data();
    Operation *rtoStop = runtimeOperations.data() + length;
    MPruntimeTriad *rta = MPoperands.data();
#ifdef COMPLEX
    mpfr_t temp1, temp2, temp3, denom;
    mpfr_init(temp1);
    mpfr_init(temp2);
    mpfr_init(temp3);
    mpfr_init(denom);
    MPfloat temp;
    mpfr_init(temp.re);
    mpfr_init(temp.im);
    MPfloat u;
    mpfr_init(u.re);
    mpfr_init(u.im);
    MPfloat v;
    mpfr_init(v.re);
    mpfr_init(v.im);
    MPfloat z;
    mpfr_init(z.re);
    mpfr_init(z.im);
    mpfr_t pi;
    mpfr_init(pi);
    mpfr_const_pi(pi, GMP_RNDN);
#endif

    for (; rto < rtoStop; rto++, rta++) {
        switch (*rto) {
            case Operation::CPY:
                CPY(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::LOG:
                LOG(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::NEG:
                NEG(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::INV:
                INV(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::POW:
                POW(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.secondOperand));
                break;
            case Operation::IPOW:
                IPOW(rta->aIF.result, *(rta->aIF.firstOperand), rta->aIF.secondOperand);
                break;
            case Operation::IPOW2:
                MUL(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.firstOperand));
                break;
            case Operation::IPOW3:
                POW3(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::IPOW4:
                POW4(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::IPOW5:
                POW5(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::IPOW6:
                POW6(rta->aF.result, *(rta->aF.firstOperand));
                break;
            case Operation::MINUS:
                MINUS(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.secondOperand));
                break;
            case Operation::DIV:
                DIV(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.secondOperand));
                break;
            case Operation::PLUS:
                PLUS(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.secondOperand));
                break;
            case Operation::MUL:
                MUL(rta->aF.result, *(rta->aF.firstOperand), *(rta->aF.secondOperand));
                break;
        } /*switch(*rto)*/
    }     /*for(;rto<rtoStop;rto++,rta++)*/

#ifdef COMPLEX
    mpfr_clear(temp1);
    mpfr_clear(temp2);
    mpfr_clear(temp3);
    mpfr_clear(denom);
    mpfr_clear(pi);
    mpfr_clear(u.re);
    mpfr_clear(u.im);
    mpfr_clear(v.re);
    mpfr_clear(v.im);
    mpfr_clear(z.re);
    mpfr_clear(z.im);
    mpfr_clear(temp.re);
    mpfr_clear(temp.im);
#endif

    rta--;
    rto--;

    if (*rto == Operation::IPOW) {
        res[0] = mpfr_get_d(rta->aIF.result.re, GMP_RNDN);
#ifdef COMPLEX
        res[1] = mpfr_get_d(rta->aIF.result.im, GMP_RNDN);
#endif
    } else {
        res[0] = mpfr_get_d(rta->aF.result.re, GMP_RNDN);
#ifdef COMPLEX
        res[1] = mpfr_get_d(rta->aF.result.im, GMP_RNDN);
#endif
    }
}

void Runtime::RunlineNative(double *res, unsigned int points) {
    int local_bunch = points;
    if ((local_bunch % 4) != 0) local_bunch += (4 - (local_bunch % 4));

    Operation *rto = runtimeOperations.data();  // another branch of Operation for native
    Operation *rtoStop = runtimeOperations.data() + length;
    NativeRuntimeTriad *rta = nativeOperands.data();

    int run;
    int j;
    auto time_start = std::chrono::steady_clock::now();
#if defined(ENABLE_AVX) || defined(DOXYGEN_DOCUMENTATION)
    if (Common::avx_enabled){
#ifdef COMPLEX
        __m256d twos = _mm256_set1_pd(2.0);
        __m256d threes = _mm256_set1_pd(3.0);
        __m256d zeros = _mm256_set1_pd(0.);
#endif
#ifndef COMPLEX
#ifndef ENABLE_ICC
        __m256d eps = _mm256_set1_pd(1.0E-100);
#endif
#endif
        __m256d ones = _mm256_set1_pd(1.);
        for (; rto < rtoStop; rto++, rta++) {
            __m256d *first_operand_re, *second_operand_re, *result_re;


            __m256d *I_result_re = reinterpret_cast<__m256d *> (rta->aIPN.result);
            __m256d *I_first_operand_re = reinterpret_cast<__m256d *> (rta->aIPN.firstOperand);
#ifdef COMPLEX
            __m256d *I_first_operand_im = reinterpret_cast<__m256d *> (rta->aIPN.firstOperand + Common::batchSize);
            __m256d *I_result_im = reinterpret_cast<__m256d *> (rta->aIPN.result + Common::batchSize);
#endif
            unsigned int deg = rta->aIPN.secondOperand;
            first_operand_re = reinterpret_cast< __m256d *> (rta->aPN.firstOperand);
            second_operand_re = reinterpret_cast<__m256d *> (rta->aPN.secondOperand);
            result_re = reinterpret_cast<__m256d *> (rta->aPN.result);
#ifdef COMPLEX
            __m256d *first_operand_im, *second_operand_im, *result_im;
            first_operand_im = reinterpret_cast< __m256d *> (rta->aPN.firstOperand + Common::batchSize);
            second_operand_im = reinterpret_cast<__m256d *> (rta->aPN.secondOperand + Common::batchSize);
            result_im = reinterpret_cast<__m256d *> (rta->aPN.result + Common::batchSize);
#endif
            switch (*rto) {
                case Operation::CPY:
                    for (run = 0; run != local_bunch; run += 4){
                        result_re[run] = first_operand_re[run];
#ifdef COMPLEX
                        result_im[run] = first_operand_im[run];
#endif
                    }
                    break;
                case Operation::LOG:
#ifndef COMPLEX
#ifdef ENABLE_ICC
                    for (run = 0; run != local_bunch; run += 4){
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize); }
                    }
                    break;
#endif
#ifndef ENABLE_ICC
                    for (run = 0; run != local_bunch/4; run ++){

                        Vec4d A(_mm256_max_pd(first_operand_re[run], eps));
                        Vec4d B;
                        B = log(A);

                        result_re[run] = B;
                    }
                    break;
#endif
#endif

#ifdef COMPLEX
                    for (run = 0; run != local_bunch; run += 4){
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize); }
                        { NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize); }
                    }
                    break;
#endif

                case Operation::NEG:
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = -first_operand_re[run];
#ifdef COMPLEX
                        result_im[run] = -first_operand_im[run];
#endif
                    }
                    break;
                case Operation::INV:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_div_pd(ones, first_operand_re[run]);
                    }
#else
                    __m256d temp;
                    for (run = 0; run != local_bunch/4; run ++){
                        temp = _mm256_add_pd(_mm256_mul_pd(first_operand_re[run], first_operand_re[run]),
                                _mm256_mul_pd(first_operand_im[run], first_operand_im[run]));
                        result_re[run] = _mm256_div_pd(first_operand_re[run], temp);
                        result_im[run] = _mm256_div_pd(-first_operand_im[run], temp);
                    }
#endif
                    break;
                case Operation::IPOW2:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_mul_pd(first_operand_re[run], first_operand_re[run]);
                    }
#else
                    __m256d c_pow21, c_pow22;

                    for (run = 0; run != local_bunch/4; run ++){
                        c_pow21 = _mm256_mul_pd(first_operand_re[run], first_operand_re[run]);
                        c_pow22 = _mm256_mul_pd(first_operand_im[run], first_operand_im[run]);
                        result_re[run] = _mm256_sub_pd(c_pow21, c_pow22);
                        result_im[run] = _mm256_mul_pd(twos, _mm256_mul_pd(first_operand_re[run], first_operand_im[run]));
                    }
#endif
                    break;
                case Operation::IPOW3:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_mul_pd(_mm256_mul_pd(first_operand_re[run], first_operand_re[run]),first_operand_re[run]);
                    }
#else
                    __m256d pow31_re_sqr, pow31_im_sqr;
                    for (run = 0; run != local_bunch/4; run ++){

                        pow31_re_sqr = _mm256_mul_pd(first_operand_re[run], first_operand_re[run]);
                        pow31_im_sqr = _mm256_mul_pd(first_operand_im[run], first_operand_im[run]);

                        result_re[run] = _mm256_sub_pd(_mm256_mul_pd(pow31_re_sqr, first_operand_re[run]),
                                _mm256_mul_pd(threes, _mm256_mul_pd(pow31_im_sqr, first_operand_re[run])));

                        result_im[run] = _mm256_sub_pd(_mm256_mul_pd(threes, _mm256_mul_pd(pow31_re_sqr, first_operand_im[run])),
                                _mm256_mul_pd(pow31_im_sqr, first_operand_im[run]));
                    }
#endif
                    break;
                case Operation::IPOW4:
#ifndef COMPLEX
                    __m256d temp;
                    for (run = 0; run != local_bunch/4; run ++){
                        temp = _mm256_mul_pd(first_operand_re[run], first_operand_re[run]);
                        result_re[run] = _mm256_mul_pd(temp, temp);
                    }
#else
                    __m256d tre, tim;
                    for (run = 0; run != local_bunch/4; run++){
                        tre = _mm256_sub_pd(_mm256_mul_pd(first_operand_re[run], first_operand_re[run]),
                                _mm256_mul_pd(first_operand_im[run], first_operand_im[run]));
                        tim = _mm256_mul_pd(twos, _mm256_mul_pd(first_operand_re[run], first_operand_im[run]));

                        result_re[run] = _mm256_sub_pd(_mm256_mul_pd(tre, tre), _mm256_mul_pd(tim, tim));
                        result_im[run] = _mm256_mul_pd(twos, _mm256_mul_pd(tre, tim));
                    }
#endif
                    break;
                case Operation::IPOW5:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW6:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        temp = _mm256_mul_pd(first_operand_re[run], first_operand_re[run]);
                        result_re[run] = _mm256_mul_pd(_mm256_mul_pd(temp, temp), temp);
                    }
#else
                    for (run = 0; run != local_bunch/4; run ++){
                        tre = _mm256_sub_pd(_mm256_mul_pd(first_operand_re[run], first_operand_re[run]),
                                _mm256_mul_pd(first_operand_im[run], first_operand_im[run]));
                        tim = _mm256_mul_pd(twos, _mm256_mul_pd(first_operand_im[run], first_operand_re[run]));
                        result_re[run] = _mm256_sub_pd(_mm256_mul_pd(_mm256_mul_pd(tre, tre), tre),
                                _mm256_mul_pd(threes, _mm256_mul_pd(_mm256_mul_pd(tre, tim), tim)));
                        result_im[run] = _mm256_sub_pd(_mm256_mul_pd(threes, _mm256_mul_pd(_mm256_mul_pd(tre, tre), tim)),
                                _mm256_mul_pd(_mm256_mul_pd(tim, tim), tim));
                    }
#endif
                    break;
                case Operation::IPOW:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        deg = rta->aIPN.secondOperand;
                        if (deg == 2){
                            I_result_re[run] = _mm256_mul_pd(I_first_operand_re[run], I_first_operand_re[run]);
                        }
                        else{
                            if ((deg & 1) != 0)
                                I_result_re[run] = I_first_operand_re[run];
                            else
                                I_result_re[run] = ones; //{1.0, 1.0, 1.0, 1.0}
                            __m256d z;
                            z = I_first_operand_re[run];
                            deg >>= 1;
                            while (deg){
                                z = _mm256_mul_pd(z, z);
                                if ((deg & 1) != 0) I_result_re[run] = _mm256_mul_pd(I_result_re[run], z);
                                deg >>= 1;
                            }
                        }
                    }
#else
                    for (run = 0; run != local_bunch/4; run ++){
                        deg = rta->aIPN.secondOperand;
                        __m256d bre = I_first_operand_re[run];
                        __m256d bim = I_first_operand_im[run];
                        if (deg == 2){
                            I_result_re[run] = _mm256_sub_pd(_mm256_mul_pd(bre, bre), _mm256_mul_pd(bim, bim));
                            I_result_im[run] = _mm256_mul_pd(twos, _mm256_mul_pd(bre, bim));
                        }
                        else{
                            __m256d are, aim;
                            if ((deg & 1) != 0){
                                are = bre;
                                aim = bim;
                            }
                            else{
                                are = ones;
                                aim = zeros;
                            }
                            deg >>= 1;
                            __m256d t;
                            while (deg){
                                t = _mm256_sub_pd(_mm256_mul_pd(bre, bre), _mm256_mul_pd(bim, bim));
                                bim  = _mm256_mul_pd(twos, _mm256_mul_pd(bre, bim));
                                bre = t;
                                if ((deg & 1) != 0){
                                    t = _mm256_sub_pd(_mm256_mul_pd(are, bre), _mm256_mul_pd(aim, bim));
                                    aim = _mm256_add_pd(_mm256_mul_pd(are, bim), _mm256_mul_pd(aim, bre));
                                    are = t;
                                }
                                deg >>= 1;
                            }
                            I_result_re[run] = are;
                            I_result_im[run] = aim;
                        }
                    }
#endif
                    break;
                case Operation::PLUS:
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_add_pd(first_operand_re[run], second_operand_re[run]);
#ifdef COMPLEX
                        result_im[run] = _mm256_add_pd(first_operand_im[run], second_operand_im[run]);
#endif
                    }
                    break;
                case Operation::MUL:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_mul_pd(first_operand_re[run], second_operand_re[run]);
                    }
#else
                    __m256d mul_help1, mul_help2, mul_help3, mul_help4;

                    for (run = 0; run != local_bunch/4; run ++){
                        mul_help1 = _mm256_mul_pd(first_operand_re[run], second_operand_re[run]);
                        mul_help2 = _mm256_mul_pd(first_operand_im[run], second_operand_im[run]);
                        mul_help3 = _mm256_mul_pd(first_operand_im[run], second_operand_re[run]);
                        mul_help4 = _mm256_mul_pd(first_operand_re[run], second_operand_im[run]);
                        result_re[run] = _mm256_sub_pd(mul_help1, mul_help2);
                        result_im[run] = _mm256_add_pd(mul_help3, mul_help4);
                    }
#endif
                    break;
                case Operation::DIV:
#ifndef COMPLEX
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_div_pd(first_operand_re[run], second_operand_re[run]);
                    }
#else
                    __m256d re_numerator, im_numerator, denumerator;
                    for (run = 0; run != local_bunch/4; run ++){
                        denumerator = _mm256_add_pd(_mm256_mul_pd(second_operand_re[run], second_operand_re[run]),
                                _mm256_mul_pd(second_operand_im[run], second_operand_im[run]));
                        re_numerator = _mm256_add_pd(_mm256_mul_pd(first_operand_re[run], second_operand_re[run]),
                                _mm256_mul_pd(first_operand_im[run], second_operand_im[run]));
                        im_numerator = _mm256_sub_pd(_mm256_mul_pd(first_operand_im[run], second_operand_re[run]),
                                _mm256_mul_pd(first_operand_re[run], second_operand_im[run]));
                        result_re[run] = _mm256_div_pd(re_numerator, denumerator);
                        result_im[run] = _mm256_div_pd(im_numerator, denumerator);
                    }

#endif
                    break;
                case Operation::MINUS:
                    for (run = 0; run != local_bunch/4; run ++){
                        result_re[run] = _mm256_sub_pd(first_operand_re[run], second_operand_re[run]);
#ifdef COMPLEX
                        result_im[run] = _mm256_sub_pd(first_operand_im[run], second_operand_im[run]);
#endif
                    }
                    break;
                case Operation::POW:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3, Common::batchSize);}
                    } break;
            } /*switch(*rto)*/
        }    /*for(;rto<rtoStop;rto++,rta++)*/
    }
    else{
#endif
        for (; rto < rtoStop; rto++, rta++) {
            switch (*rto) {
                case Operation::CPY:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_CPY(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_CPY(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_CPY(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_CPY(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::LOG:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_LOG(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::NEG:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_NEG(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_NEG(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_NEG(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_NEG(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    }
                    break;
                case Operation::INV:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_INV(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_INV(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_INV(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_INV(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW2:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW2(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW2(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW2(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW2(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    }
                    break;
                case Operation::IPOW3:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW3(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW3(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW3(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW3(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW4:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW4(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW4(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW4(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW4(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW5:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW5(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW6:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW6(rta->aPN.result, rta->aPN.firstOperand, run + 0, Common::batchSize);}
                        {NP_POW6(rta->aPN.result, rta->aPN.firstOperand, run + 1, Common::batchSize);}
                        {NP_POW6(rta->aPN.result, rta->aPN.firstOperand, run + 2, Common::batchSize);}
                        {NP_POW6(rta->aPN.result, rta->aPN.firstOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::IPOW:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_IPOW(rta->aIPN.result, rta->aIPN.firstOperand, rta->aIPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_IPOW(rta->aIPN.result, rta->aIPN.firstOperand, rta->aIPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_IPOW(rta->aIPN.result, rta->aIPN.firstOperand, rta->aIPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_IPOW(rta->aIPN.result, rta->aIPN.firstOperand, rta->aIPN.secondOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::PLUS:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_PLUS(rta->aPN. result,rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_PLUS(rta->aPN. result,rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_PLUS(rta->aPN. result,rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_PLUS(rta->aPN. result,rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3, Common::batchSize);}
                    }
                    break;
                case Operation::MUL:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_MUL(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_MUL(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_MUL(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_MUL(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3, Common::batchSize);}
                    }
                    break;

                case Operation::DIV:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_DIV(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_DIV(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_DIV(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_DIV(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3, Common::batchSize);}
                    } break;
                case Operation::MINUS:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_MINUS(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0,Common::batchSize);}
                        {NP_MINUS(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1,Common::batchSize);}
                        {NP_MINUS(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2,Common::batchSize);}
                        {NP_MINUS(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3,Common::batchSize);}
                    }
                    break;
                case Operation::POW:
                    for (run = 0; run != local_bunch; run += 4){
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 0, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 1, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 2, Common::batchSize);}
                        {NP_POW(rta->aPN.result, rta->aPN.firstOperand, rta->aPN.secondOperand, run + 3, Common::batchSize);}
                    } break;
            } /*switch(*rto)*/
        }     /*for(;rto<rtoStop;rto++,rta++)*/
#if defined(ENABLE_AVX) || defined(DOXYGEN_DOCUMENTATION)
    }
#endif
    rta--;
    rto--;

    for (j = 0; j != local_bunch; ++j) {
        if (*rto == Operation::IPOW) {
            (res)[j] = rta->aIPN.result[j];
#ifdef COMPLEX
            (res)[j + Common::batchSize] = rta->aIPN.result[j + Common::batchSize];
#endif
        } else {
            (res)[j] = rta->aPN.result[j];
#ifdef COMPLEX
            (res)[j + Common::batchSize] = rta->aPN.result[j + Common::batchSize];
#endif
        }
    }
    auto time_stop = std::chrono::steady_clock::now();
    Common::timeStatistics[3] += std::chrono::duration_cast<std::chrono::microseconds>(time_stop - time_start).count();
}

/**
 * Determines x to the power y
 * @param x the variable
 * @param y exponent
 * @return x^y
 */
static inline double ipow(double x, unsigned int y) {
    double u;
    if (y == 2)
        u = x * x;
    else {
        if ((y & 1) != 0)
            u = x;
        else
            u = 1.0L;
        double z = x;
        y >>= 1;
        while (y) {
            z = z * z;
            if ((y & 1) != 0) u *= z;
            y >>= 1;
        }
    }
    return u;
}

bool Runtime::IsArithmeticNative(double *x, double *lProd) {
    *lProd = 1.0;
    if (theScan->maxX[0] > 0) {
        for (unsigned int i = 1; i < theScan->nx; i++) {
            if (theScan->maxX[i] > 0) *lProd *= ipow(x[i], theScan->maxX[i]);
        }
        if (*lProd >= Common::MPthreshold) return true;
        return false;
    }
    return true;
}

void Runtime::CreateTruncatedX(double *lProd, double *xLocal) {
    double mpsmallX = Common::MPsmallX / 10.0l, ep = absMonomMin / 10.0l;
    unsigned int i;

    double xLocal2[maxVariables]; // a single point - modified coordinates

    if (*lProd < absMonomMin) {
        if (mpsmallX < absMonomMin) mpsmallX = absMonomMin;

        for (i = 1; i < theScan->nx; i++)
            if (xLocal[i] < mpsmallX) {
                xLocal2[i] = (mpsmallX - xLocal[i]) / 2.0;
            } else {
                xLocal2[i] = 0.0l;
            }

        int n = 0;
        do {
            n++;
            *lProd = 1.0;
            for (i = 1; i < theScan->nx; i++) (*lProd) *= ipow(xLocal[i] + xLocal2[i], theScan->maxX[i]);
            if ((*lProd) < absMonomMin) {
                for (i = 1; i < theScan->nx; i++) xLocal2[i] *= 1.5l;
            } else {
                for (i = 1; i < theScan->nx; i++) xLocal2[i] *= 0.5l;
            }
            if ((n >= 128) && ((*lProd) >= absMonomMin)) break;
            if (n == 256) break;
        } while (((*lProd) < absMonomMin) || ((*lProd) - absMonomMin > ep));

    } else {
        for (i = 1; i < theScan->nx; i++) {  // no need to truncate, 0 shift
            xLocal2[i] = 0;
        }
    }

    for (i = 1; i < theScan->nx; i++) {
        xLocal[i] += xLocal2[i];  // now initial data added
    }
}

void Runtime::SetMPPrecision(int precision) {
    MPPrecision = precision;
    mpfr_set_default_prec(precision);
    for (auto itr : theScan->allMPAddresses) {
        mpfr_prec_round(itr->re, precision, GMP_RNDN);
#ifdef COMPLEX
        mpfr_prec_round(itr->im, precision, GMP_RNDN);
#endif
    }
    ResetMPConstants();
}

static inline int intlog2(double x) { return ceil(log(x) * 1.44269504088896l); }

void Runtime::RunSingeMPFRExpr(double *res, double *lProd, double* xLocal) {
    auto timeStart = std::chrono::steady_clock::now();

    unsigned int i;

    CreateTruncatedX(lProd, xLocal);  // modifies lProd and xLocal
    if (*lProd >= minimalMPDefault) {
        // lProd is big enough for default MP precision
        if (MPPrecision != defaultMPPrecision) {
            SetMPPrecision(defaultMPPrecision);
        }
    } else {
        //lProd is small, logarithm is negative
        int newPrec = -intlog2(*lProd) + Common::MPprecisionShift;
        // defaultMPPrecision = -intlog2(minimalMPDefault) + Common::MPprecisionShift
        // thus if (lProd < minimalMPDefault) then (intlog2(lprod) < intlog2(minimalMPDefault))
        // and (newPrec > defaultMPPrecision)
        // now if it is more than the currently used precision (at least defaultMPPrecision), then we increase it more
        if (MPPrecision < newPrec) {
            SetMPPrecision(newPrec);
        }
    }

    for (i = 1; i < theScan->nx; i++) {
        mpfr_set_d(theScan->MPx[i].re, xLocal[i], GMP_RNDN);
#ifdef COMPLEX
        mpfr_set_d(theScan->MPx[i].im, 0.0, GMP_RNDN);
#endif
    }

    RunlineMP(res);

    auto timeStop = std::chrono::steady_clock::now();
    Common::timeStatistics[5] += std::chrono::duration_cast<std::chrono::microseconds>(timeStop - timeStart).count();
}

void Runtime::RunExpr(const double *x, double *res, unsigned int numTotal) {
    x--;
    unsigned int i;
    unsigned int j;
    double xLocal[maxVariables]; // a single point
    double lProd;
    auto timeStart = std::chrono::steady_clock::now();

    unsigned int numNative = 0;  // the number of native points in this bunch
    for (j = 0; j != numTotal; ++j) {
        bool currentNative = false;                                  // whether the current x from the batch is native
        lProd = 0.;                                                   // it will be filled by isArithmeticNative in the mixed or mpfr branch
        if (Common::arithmetics == ArithmeticsMode::native) {
            currentNative = true;
        } else {
            for (i = 1; i < theScan->nx; i++) {
                xLocal[i] = x[i + (j * (theScan->nx - 1))];  // fill array for one set of x
            }
            int returnFromIsArithmeticNative = IsArithmeticNative(xLocal, &lProd);  // we check whether this point is native and calculate lProd
            if (Common::arithmetics == ArithmeticsMode::mixed) {
                currentNative = returnFromIsArithmeticNative;
            }
        }
        if (currentNative) {
            for (i = 1; i < theScan->nx; i++) {
                theScan->nativeX[(i * Common::batchSize * complexMultiplier) + numNative] = x[i + (j * (theScan->nx - 1))];
#ifdef COMPLEX
                theScan->nativeX[(i * Common::batchSize * complexMultiplier) + Common::batchSize + numNative] = 0.;
#endif
            }
            answerPosition[numNative] = j;
            numNative++;
        } else {
            RunSingeMPFRExpr(res + (complexMultiplier * j), &lProd, xLocal);
        }
    }

    Common::statistics[0] += numNative;
    Common::statistics[1] += (numTotal - numNative);

#ifdef GPU
    if (Common::GPUCores) {
        auto timeStart2 = std::chrono::steady_clock::now();
        MEASURE_GPU_TIME((SAFE_CALL((cudaMemcpy(nativeXGPU, &theScan->nativeX[Common::batchSize * complexMultiplier], GetDimension() * Common::batchSize * sizeof(double) * complexMultiplier,
                                                cudaMemcpyHostToDevice)))),
                         0);
        MEASURE_GPU_TIME(
            RunlineNativeGPU(numNative, Common::batchSize, length, firstOperandGPU, secondOperandGPU, secondOperandIntegerGPU, resultGPU, operationGPU), 1
        );
        MEASURE_GPU_TIME((SAFE_CALL((cudaMemcpy(answer.data(), answerGPU, Common::batchSize * sizeof(double) * complexMultiplier, cudaMemcpyDeviceToHost)))),
                         0);
        cudaDeviceSynchronize();
        auto timeStop2 = std::chrono::steady_clock::now();
        Common::timeStatistics[1] += std::chrono::duration_cast<std::chrono::microseconds>(timeStop2 - timeStart2).count();
    } else {
#endif
        unsigned int numNativeFix = (numNative / 4) * 4;
        while (numNativeFix > numNative) {
            for (i = 1; i < theScan->nx; i++) {
                theScan->nativeX[(i * Common::batchSize * complexMultiplier) + numNativeFix - 1] = theScan->nativeX[(i * Common::batchSize * complexMultiplier) + numNative - 1];
#ifdef COMPLEX
                theScan->nativeX[(i * Common::batchSize * complexMultiplier) + Common::batchSize + numNativeFix - 1] = 0.;
#endif
            }
            --numNativeFix;
        }
        RunlineNative(answer.data(), numNative);
#ifdef GPU
    }
#endif

#ifndef COMPLEX
    for (j = 0; j != numNative; ++j) res[(answerPosition[j]) + 0] = answer[j];
#else
    for (j = 0; j != numNative; ++j) res[(2 * answerPosition[j]) + 0] = answer[j];
    for (j = 0; j != numNative; ++j) res[(2 * answerPosition[j]) + 1] = answer[j + Common::batchSize];
#endif

    if (Common::debug) {
        for (j = 0; j != numTotal; ++j) {
            printf("point: {");
            for (i = 1; i <= ((theScan->nx - 1)); i++) {
                printf("%f, ", x[i + (j * (theScan->nx - 1))]);
            }
            printf("} ");
            printf("result: ");
#ifndef COMPLEX
            printf("{%.15f}", res[j]);
#else
            printf("{%.15f,%.15f}", res[2 * j + 0], res[2 * j + 1]);
#endif
            printf("\n");
        }
    }

#ifdef COMPLEX
    if (Common::testF) {
        for (j = 0; j != numTotal; ++j) {
            if (res[(2 * j) + 1] > 0) {
                fprintf(stderr,"{");
                for (i = 1; i < theScan->nx; i++) {
                    fprintf(stderr,"%f, ", x[i + (j * (theScan->nx - 1))]);
                }
                fprintf(stderr,"} result: ");
                fprintf(stderr,"{%.15f,%.15f}\n", res[(2 * j) + 0], res[(2 * j) + 1]);
            }
        }
    }
#endif
    auto timeStop = std::chrono::steady_clock::now();
    Common::timeStatistics[2] += std::chrono::duration_cast<std::chrono::microseconds>(timeStop - timeStart).count();
}

#ifdef GPU
static unsigned int GetPhysicalMemory(unsigned int memory) {
    const unsigned int pageSize = 1048576;
    return ((memory - 1) / pageSize + 1) * pageSize;
}

static unsigned int EstimateMemory(int ops, int ops2, int X) {
    return GetPhysicalMemory(ops * Common::batchSize * sizeof(double) * complexMultiplier) + GetPhysicalMemory(ops2 * Common::batchSize * sizeof(double) * complexMultiplier) +
           GetPhysicalMemory(X * Common::batchSize * sizeof(double) * complexMultiplier);
}
#endif

MPfloat* Runtime::GetMPAddress(Operand op) {
    switch (op.operandType) {
        case OperandType::Triad:
            return &(MPoperands[op.operandNumber].aF.result);
        case OperandType::x:
            return &theScan->MPx[op.operandNumber];
        case OperandType::f:
            return GetMPAddress(theScan->f[op.operandNumber]);
        case OperandType::constant:
            return &theScan->MPConstants[op.operandNumber];
        default:
            FIESTA_FAIL("Unknown Operand type");
    }
}

double* Runtime::GetNativeAddress(Operand op, const std::vector<unsigned int>& triadReusedNumber, int originalBatchSize) {
    switch (op.operandType) {
        case OperandType::Triad: {
            unsigned int res = triadReusedNumber[op.operandNumber];
            auto offset = (res % theScan->triadBlockSize) * Common::batchSize * complexMultiplier;
            return &theScan->nativeTriadAddresses[res / theScan->triadBlockSize][offset];
        }
        case OperandType::x:
            return &theScan->nativeX[op.operandNumber * Common::batchSize * complexMultiplier];
        case OperandType::f:
            return GetNativeAddress(theScan->f[op.operandNumber], triadReusedNumber, originalBatchSize);
        case OperandType::constant:
            return theScan->nativeConstants[op.operandNumber].get();
        default:
            FIESTA_FAIL("Unknown Operand type");
    }
}

#if defined(GPU) || defined(DOXYGEN_DOCUMENTATION)
double* Runtime::GetGPUAddress(Operand op, const std::vector<unsigned int>& triadReusedNumber) {
    switch (op.operandType) {
        case OperandType::Triad: {
            return GPUtriadAddresses[triadReusedNumber[op.operandNumber]];
        }
        case OperandType::x:
            return nativeXGPU + (Common::batchSize * (op.operandNumber - 1) * complexMultiplier);
        case OperandType::f:
            return GetGPUAddress(theScan->f[op.operandNumber], triadReusedNumber);
        case OperandType::constant:
            return constantsGPU[op.operandNumber];
        default:
            FIESTA_FAIL("Unknown Operand type");
    }
}
#endif

void Runtime::ResetMPConstants() {
    for (unsigned j = 0; j < theScan->MPConstants.size(); j++) {
        if (theScan->stringConstants[j] == "") {
            // that's an inverted constant following the real one
            mpfr_div(theScan->MPConstants[j].re, theScan->MPConstants[1].re, theScan->MPConstants[j - 1].re, GMP_RNDN);
#ifdef COMPLEX
            mpfr_set_d(theScan->MPConstants[j].im, 0.0, GMP_RNDN);
#endif
        } else {
            const char *str = theScan->stringConstants[j].c_str();
            if ((str[0] == 'I') && (str[1] == '\0')) {
#ifdef COMPLEX
                mpfr_set_d(theScan->MPConstants[j].re, 0.0, GMP_RNDN);
                mpfr_set_d(theScan->MPConstants[j].im, 1.0, GMP_RNDN);
#else
                FIESTA_FAIL("I constant encountered in real mode");
#endif
            } else {
                const char *pos = str;
                // This is an advanced way of setting constants in case it is a big fraction
                while ((*pos != '/') && (*pos != '\0') && (pos - str < 10)) ++pos;
                if (*pos == '/') {
                    ++pos;
                    mpfr_t nom, denom;
                    mpfr_init(nom);
                    mpfr_init(denom);
                    mpfr_set_str(nom, str, 10, GMP_RNDN);
                    mpfr_set_str(denom, pos, 10, GMP_RNDN);
                    mpfr_div(theScan->MPConstants[j].re, nom, denom, GMP_RNDN);
                    mpfr_clear(nom);
                    mpfr_clear(denom);
                } else {
                    mpfr_set_str(theScan->MPConstants[j].re, str, 10, GMP_RNDN);
                }
#ifdef COMPLEX
                    mpfr_set_d(theScan->MPConstants[j].im, 0.0, GMP_RNDN);
#endif
            }
        }
    }
}

void Runtime::BuildRuntime() {

    // let us determine MP mode precision
    //  calculate the product in the point of small X
    double prod = 1.0;
    if (theScan->maxX[0] > 0) {
        for (unsigned int i = 1; i < theScan->nx; i++) {
            if (theScan->maxX[i] > 0) prod *= ipow(Common::MPsmallX, theScan->maxX[i]);
        }
    }

    // if the product is big enough, we work with default values
    if (prod > Common::MPthreshold) prod = Common::MPthreshold / 2.0;

    if (prod < absMonomMin) prod = absMonomMin; /*To avoid overflow*/

    if (Common::userSetMPMin > 0.) {
        minimalMPDefault = Common::userSetMPMin;
    } else {
        minimalMPDefault = prod;
    }

    defaultMPPrecision = -intlog2(minimalMPDefault) + Common::MPprecisionShift;
    SetMPPrecision(defaultMPPrecision);

    unsigned int triadsCounter = 0;
    unsigned int triadsNumber = theScan->compileTimeTriads.size();

    answerPosition.resize(Common::batchSize);                  // positions of answers from the native bunch
    answer.resize(Common::batchSize * complexMultiplier);  // answers for batch

    std::stack<unsigned int> tlineStack; // stack of triads that are no longer needed

    std::vector<unsigned int> lastUsing(theScan->compileTimeTriads.size(), 0); // last Triad using the current one

    for (triadsCounter = 0; triadsCounter < triadsNumber; ++triadsCounter) {
        Operand op1 = theScan->compileTimeTriads[triadsCounter].firstOperand;
        while (op1.operandType == OperandType::f) op1 = theScan->f[op1.operandNumber];
        if (op1.operandType == OperandType::Triad) {
            if (lastUsing[op1.operandNumber] < triadsCounter) {
                lastUsing[op1.operandNumber] = triadsCounter;
            }
        }
        if (
            (theScan->compileTimeTriads[triadsCounter].operation != Operation::POW) &&
            (theScan->compileTimeTriads[triadsCounter].operation != Operation::MUL) &&
            (theScan->compileTimeTriads[triadsCounter].operation != Operation::DIV) &&
            (theScan->compileTimeTriads[triadsCounter].operation != Operation::PLUS) &&
            (theScan->compileTimeTriads[triadsCounter].operation != Operation::MINUS)
           ) continue;
        Operand op2 = theScan->compileTimeTriads[triadsCounter].secondOperand;
        while (op2.operandType == OperandType::f) op2 = theScan->f[op2.operandNumber];
        if ((op1 != op2) && (op2.operandType == OperandType::Triad)) {
            if (lastUsing[op2.operandNumber] < triadsCounter) {
                lastUsing[op2.operandNumber] = triadsCounter;
            }
        }
    }

    std::vector<std::pair<unsigned int,unsigned int>> lastUsed(theScan->compileTimeTriads.size()); // maximally two triads for which the current one is the last using

    // from lastUsing to lastUsed
    for (triadsCounter = 0; triadsCounter < triadsNumber; triadsCounter++) {
        if (lastUsing[triadsCounter] != 0) {
            if (lastUsed[lastUsing[triadsCounter]].first == 0) {
                lastUsed[lastUsing[triadsCounter]].first = triadsCounter;
            } else {
                lastUsed[lastUsing[triadsCounter]].second = triadsCounter;
            }
        }
    }

    std::vector<unsigned int> triadReusedNumber(theScan->compileTimeTriads.size(), 0);
    // each Triad will get a number, many of them will be reused because the life-span of a Triad can be over
    Common::memoryTetrads = 0;

    for (triadsCounter = 0; triadsCounter < triadsNumber; triadsCounter++) {
        if (!tlineStack.empty()) {  // reusing a Triad
            triadReusedNumber[triadsCounter] = tlineStack.top();
            tlineStack.pop();
        } else {  // new slot
            triadReusedNumber[triadsCounter] = Common::memoryTetrads;
            Common::memoryTetrads++;
        }
        if (lastUsed[triadsCounter].first != 0) {
            unsigned int ind = triadReusedNumber[lastUsed[triadsCounter].first];
            if (ind > 0) {
                tlineStack.push(ind);
            }
        }
        if (lastUsed[triadsCounter].second != 0) {
            unsigned int ind = triadReusedNumber[lastUsed[triadsCounter].second];
            if (ind > 0) {
                tlineStack.push(ind);
            }
        }
    }

    theScan->nativeTriadAddresses.reserve((Common::memoryTetrads/theScan->triadBlockSize) + 1);

    // we break it into slots
    for (size_t j = 0; j * theScan->triadBlockSize < Common::memoryTetrads; j++) {
        theScan->nativeTriadAddresses.emplace_back(
            std::unique_ptr<double[], decltype(&Scanner::alignedDeleter)>(
                new(Scanner::AVXAlign) double[theScan->triadBlockSize * Common::batchSize * complexMultiplier],
                &Scanner::alignedDeleter
            )
        );
    }

    int originalBatchSize = Common::batchSize; // GubaBatch can be decreased in GPU mode for memory reasons, but with should use proper addresses in native line

#ifdef GPU
    if (Common::GPUCores) {
        // calculating whether we should decrease the number of blocks
        long long int neededRam = EstimateMemory(Common::memoryTetrads, theScan->nativeConstants.size(), theScan->nx) * 1.1;

        long long int neededRamExtra =
            GetPhysicalMemory((4 * sizeof(double *)) * (theScan->compileTimeTriads.size() + 1)) + GetPhysicalMemory((1 * sizeof(char)) * (theScan->compileTimeTriads.size() + 1)) + 100000;

        long long int ramFree = (Common::cudaMemoryLimit / Common::GPUMemoryPart - neededRamExtra);
        if (ramFree < neededRam) {
            // decreasing arrays with native numbers
            Common::batchSize = cudaBlockSize * Common::cudaSM * (cudaBLocksPerSM * ramFree / neededRam);
            neededRam = EstimateMemory(Common::memoryTetrads, theScan->nativeConstants.size(), theScan->nx) * 1.1;
            while (ramFree < neededRam) {
                Common::batchSize -= cudaBlockSize * Common::cudaSM;
                neededRam = EstimateMemory(Common::memoryTetrads, theScan->nativeConstants.size(), theScan->nx) * 1.1;
            }
        }
    }
#endif

    theScan->nativeX = std::unique_ptr<double[], decltype(&Scanner::alignedDeleter)>(
            new(Scanner::AVXAlign) double[theScan->nx * Common::batchSize * complexMultiplier],
            &Scanner::alignedDeleter
        );
    theScan->MPx = std::vector<MPfloat>(theScan->nx);
    for (auto &var : theScan->MPx) {
        theScan->InitMPVariable(&var);
    }

    Operation *rtoN = runtimeOperations.data();
    MPruntimeTriad *rta = MPoperands.data();
    NativeRuntimeTriad *rtaN = nativeOperands.data();
#ifdef GPU
    std::vector<double*> firstOperandLocal(Common::GPUCores ? length : 0);
    std::vector<double*> secondOperandLocal(Common::GPUCores ? length : 0);
    std::vector<unsigned int> secondOperandIntegerLocal(Common::GPUCores ? length : 0);
    std::vector<double*> resultLocal(Common::GPUCores ? length : 0);
    std::vector<Operation> operationLocal(Common::GPUCores ? length : 0);
    if (Common::GPUCores) {
        // allocating memory for X
        SAFE_CALL((cudaMalloc(&nativeXGPU, GetDimension() * sizeof(double) * Common::batchSize * complexMultiplier)));
        constantsGPU.resize(length);
        for (size_t j = 0; j != theScan->nativeConstants.size(); ++j) {
            SAFE_CALL((cudaMalloc(constantsGPU.data() + j, Common::batchSize * sizeof(double) * complexMultiplier)));
            SAFE_CALL((cudaMemcpy(constantsGPU[j], theScan->nativeConstants[j].get(), Common::batchSize * sizeof(double) * complexMultiplier, cudaMemcpyHostToDevice)));
        }
        GPUtriadAddresses.reserve(Common::memoryTetrads);
        for (size_t j = 0; j != Common::memoryTetrads; ++j) {
            SAFE_CALL((cudaMalloc(GPUtriadAddresses.data() + j, Common::batchSize * sizeof(double) * complexMultiplier)));
        }
    }

#endif

    for (triadsCounter = 0; triadsCounter < triadsNumber; triadsCounter++) {
        if (theScan->compileTimeTriads[triadsCounter].operation == Operation::IPOW) {
            /*Operation::IPOW requires integer for a second argument:*/
            rta->aIF.firstOperand = GetMPAddress(theScan->compileTimeTriads[triadsCounter].firstOperand);
            FIESTA_VERIFY(
                theScan->compileTimeTriads[triadsCounter].secondOperand.operandType == OperandType::integer,
                "incorrect Operand type for IPOW");
            rta->aIF.secondOperand = theScan->compileTimeTriads[triadsCounter].secondOperand.operandNumber;
            rtaN->aIPN.firstOperand = GetNativeAddress(theScan->compileTimeTriads[triadsCounter].firstOperand, triadReusedNumber, originalBatchSize);
            rtaN->aIPN.secondOperand = theScan->compileTimeTriads[triadsCounter].secondOperand.operandNumber;
#ifdef GPU
            if (Common::GPUCores) {
                firstOperandLocal[triadsCounter] = GetGPUAddress(theScan->compileTimeTriads[triadsCounter].firstOperand, triadReusedNumber);
                secondOperandIntegerLocal[triadsCounter] = rtaN->aIPN.secondOperand;
                resultLocal[triadsCounter] = GPUtriadAddresses[triadReusedNumber[triadsCounter]];
            }
#endif
            theScan->InitMPVariable(&(rta->aIF.result));
            auto offset = (triadReusedNumber[triadsCounter] % theScan->triadBlockSize) * Common::batchSize * complexMultiplier;
            rtaN->aIPN.result = &theScan->nativeTriadAddresses[triadReusedNumber[triadsCounter] / theScan->triadBlockSize][offset];
        } else {
            rta->aF.firstOperand = GetMPAddress(theScan->compileTimeTriads[triadsCounter].firstOperand);
            rtaN->aPN.firstOperand = GetNativeAddress(theScan->compileTimeTriads[triadsCounter].firstOperand, triadReusedNumber, originalBatchSize);
            if ((theScan->compileTimeTriads[triadsCounter].operation == Operation::POW) || (theScan->compileTimeTriads[triadsCounter].operation == Operation::PLUS) || (theScan->compileTimeTriads[triadsCounter].operation == Operation::MINUS) || (theScan->compileTimeTriads[triadsCounter].operation == Operation::MUL) || (theScan->compileTimeTriads[triadsCounter].operation == Operation::DIV)) {
                rta->aF.secondOperand = GetMPAddress(theScan->compileTimeTriads[triadsCounter].secondOperand);
                rtaN->aPN.secondOperand = GetNativeAddress(theScan->compileTimeTriads[triadsCounter].secondOperand, triadReusedNumber, originalBatchSize);
#ifdef GPU
                if (Common::GPUCores) {
                    secondOperandLocal[triadsCounter] = GetGPUAddress(theScan->compileTimeTriads[triadsCounter].secondOperand, triadReusedNumber);
                }
#endif
            } else {
                // unary Operation
                rta->aF.secondOperand = nullptr;
                rtaN->aPN.secondOperand = nullptr;
            }
            theScan->InitMPVariable(&(rta->aF.result));
            auto offset = (triadReusedNumber[triadsCounter] % theScan->triadBlockSize) * Common::batchSize * complexMultiplier;
            rtaN->aPN.result = &theScan->nativeTriadAddresses[triadReusedNumber[triadsCounter] / theScan->triadBlockSize][offset];
#ifdef GPU
            if (Common::GPUCores) {
                firstOperandLocal[triadsCounter] = GetGPUAddress(theScan->compileTimeTriads[triadsCounter].firstOperand, triadReusedNumber);
                secondOperandIntegerLocal[triadsCounter] = 0;
                resultLocal[triadsCounter] = GPUtriadAddresses[triadReusedNumber[triadsCounter]];
            }
#endif
        }
#ifdef GPU
        if (Common::GPUCores) {
            operationLocal[triadsCounter] = theScan->compileTimeTriads[triadsCounter].operation;
        }
#endif
        rta++;
        rtaN++;
        *(rtoN++) = theScan->compileTimeTriads[triadsCounter].operation;
    }

#ifdef GPU
    if (Common::GPUCores) {

        answerGPU = resultLocal[length - 1];

        SAFE_CALL((cudaMalloc(&firstOperandGPU, sizeof(double*) * length)));
        SAFE_CALL((cudaMemcpy(firstOperandGPU, firstOperandLocal.data(), sizeof(double*) * length, cudaMemcpyHostToDevice)));

        SAFE_CALL((cudaMalloc(&secondOperandGPU, sizeof(double*) * length)));
        SAFE_CALL((cudaMemcpy(secondOperandGPU, secondOperandLocal.data(), sizeof(double*) * length, cudaMemcpyHostToDevice)));

        SAFE_CALL((cudaMalloc(&secondOperandIntegerGPU, sizeof(unsigned int) * length)));
        SAFE_CALL((cudaMemcpy(secondOperandIntegerGPU, secondOperandIntegerLocal.data(), sizeof(unsigned int) * length, cudaMemcpyHostToDevice)));

        SAFE_CALL((cudaMalloc(&resultGPU, sizeof(double*) * length)));
        SAFE_CALL((cudaMemcpy(resultGPU, resultLocal.data(), sizeof(double*) * length, cudaMemcpyHostToDevice)));

        SAFE_CALL((cudaMalloc(&operationGPU, sizeof(Operation) * length)));
        SAFE_CALL((cudaMemcpy(operationGPU, operationLocal.data(), sizeof(Operation) * length, cudaMemcpyHostToDevice)));

        SAFE_CALL((cudaEventCreate(&cudaStart)));
        SAFE_CALL((cudaEventCreate(&cudaStop)));
    }
#endif
}

#ifdef GPU
void Runtime::exitCuda() {
   
    if (Common::GPUCores) {
        size_t j;

        // free the native constants
        for (j = 0; j != theScan->nativeConstants.size(); ++j) {
            SAFE_CALL((cudaFree(constantsGPU[j])));
        }

        // free locations for x
        SAFE_CALL((cudaFree(nativeXGPU)));

        // free locations for tetrads
        for (j = 0; j != Common::memoryTetrads; ++j) {
            SAFE_CALL((cudaFree(GPUtriadAddresses[j])));
        }

        SAFE_CALL((cudaFree(firstOperandGPU)));
        SAFE_CALL((cudaFree(secondOperandGPU)));
        SAFE_CALL((cudaFree(secondOperandIntegerGPU)));
        SAFE_CALL((cudaFree(resultGPU)));
        SAFE_CALL((cudaFree(operationGPU)));

        SAFE_CALL((cudaEventDestroy(cudaStart)));
        SAFE_CALL((cudaEventDestroy(cudaStop)));

    }
    
    //   size_t total,freee;
    //  cudaMemGetInfo(&freee,&total);
    //  fprintf(stderr,"%zd,%zd\n",total,freee);
}
#endif

