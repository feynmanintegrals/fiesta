/** @file Runtime.h
 *
 *  This file is a part of the FIESTA package
 *
 *  Here the Runtime is implemented based on a scan class.
 *  After building triads we are ready to run the runExpr function evaluating the integrand in a batch of points.
 */

#pragma once

#ifdef GPU
#include "kernel.h"
#endif

#include <new>

#include "common.h"
#include "scanner.h"

/**
 * Runtime triads (without operation) for MP mode and non-IPOW operation
 */
class MPruntimeTriadGeneral {
    public:
        MPfloat *firstOperand; /**<First Operand*/
        MPfloat result; /**<Result*/
        MPfloat *secondOperand; /**<Second Operand*/
};

/**
 * Runtime triads (without operation) for MP mode and IPOW operation
 */
class MPruntimeTriadIPOW {
    public:
        MPfloat *firstOperand; /**<First Operand*/
        MPfloat result; /**<Result*/
        unsigned int secondOperand; /**<Second Operand (integer power)*/
};

/**
 * Union working both for normal Triad and integer power case (MP mode).
 */
union MPruntimeTriad {
    MPruntimeTriadGeneral aF; /**<Normal variant*/
    MPruntimeTriadIPOW aIF; /**<Integer power variant*/
};

/**
 * Runtime triads (without operation) for native mode and non-IPOW operation
 */
class NativeRuntimeTriadGeneral {
    public:
        double *firstOperand; /**<First Operand*/
        double *result; /**<Result*/
        double *secondOperand; /**<Second Operand*/
};

/**
 * Runtime triads (without operation) for native mode and IPOW operation
 */
class NativeRuntimeTriadIPOW {
    public:
        double *firstOperand; /**<First Operand*/
        double *result; /**<Result*/
        unsigned int secondOperand; /**<Second Operand (integer power)*/
};

/**
 * Union working both for normal Triad and integer power case (native mode).
 */
union NativeRuntimeTriad {
    NativeRuntimeTriadGeneral aPN; /**<Normal variant*/
    NativeRuntimeTriadIPOW aIPN; /**<Integer power variant*/
};

/**
 * Class for Runtime - function evaluation.
 * When evaluating we have a cycle through all Operations
 */
class Runtime {
    private:
        std::vector<Operation> runtimeOperations; /**<The Operations to be performes (addition and such) */
        std::vector<MPruntimeTriad> MPoperands; /**<Operands for multi-precision evaluation*/
        std::vector<NativeRuntimeTriad> nativeOperands; /**<Native operands*/
        unsigned int length; /**<Expression length (size of all vectors here coincides)*/

        std::vector<int> answerPosition;  /**< Positions of answer from the native bunch*/
        std::vector<double> answer;         /**<  Answers for a batch evaluation*/

        int MPPrecision = {}; /**<MP precision used currently for a point evaluation, is minimally defaultMPPrecision, but  can go higher*/
        int defaultMPPrecision = {}; /**<MP precision used by default for an integral, normally determined automatically*/
        double minimalMPDefault = {}; /**<Mininimal value for the worst monom when to use MPPrecision, change it for smaller*/

        std::unique_ptr<Scanner> theScan; /**<Scanner class which is a source for all Runtime structures */

        Runtime() = delete;

#if defined(GPU) || defined(DOXYGEN_DOCUMENTATION)
        double *nativeXGPU = {}; /**<Addresses of x (coordinates) on a GPU*/
        cudaEvent_t cudaStart; /**<Time mesaurement on GPU, timer start*/
        cudaEvent_t cudaStop; /**<Time mesaurement on GPU, timer stop*/
        double** firstOperandGPU = {}; /**<Addresses of first operands on a GPU*/
        double** secondOperandGPU = {}; /**<Addresses of second operands on a GPU*/
        unsigned int* secondOperandIntegerGPU = {}; /**<Addresses of integer second operands (for IPOW) on a GPU*/ 
        double** resultGPU = {}; /**<Addresses of results on a GPU*/
        Operation* operationGPU = {}; /**<Addresses of operations on a GPU*/ 
        std::vector<double*> constantsGPU = {}; /**<Addresses of predefined constants on a GPU*/
        std::vector<double*> GPUtriadAddresses = {}; /**<Addresses of triad values (reused) on a GPU*/
        double* answerGPU = {}; /**<Addresses of final integrand results on a GPU. Not allocated, filled with last reused position*/

        /**
         * Finds the address of an operand on a GPUa. Used in translation to Runtime.
         * @param op The compile-time encoded operation number
         * @param triadReusedNumber The number of the Triad in the real evaluation slot (those numers can be repeating due to Triad life-spans)
         * @return valid address for evaluation
         */
        double* GetGPUAddress(Operand op, const std::vector<unsigned int>& triadReusedNumber);
#endif

        /**
         * Finds the address of a multiprecision variable, which can be constant, x or Triad. Used in translation to Runtime.
         * @param op The compile-time encoded operation number
         * @return valid address for evaluation
         */
        MPfloat* GetMPAddress(Operand op);

        /**
         * Finds the address of a native variable, which can be constant, x or Triad. Used in translation to Runtime.
         * @param op The compile-time encoded operation number
         * @param triadReusedNumber The number of the Triad in the real evaluation slot (those numers can be repeating due to Triad life-spans)
         * @param originalBatchSize The original batch number before it was decreased due to lack of GPU memory.
         * @return valid address for evaluation
         */
        double* GetNativeAddress(Operand op, const std::vector<unsigned int>& triadReusedNumber, int originalBatchSize);

        /**
         * This function sets MP presicion to all MP constants and calls resetMPConstants
         * @param precision precision to be set
         */
        void SetMPPrecision(int precision);

        /**
         * Updating MP constants with values from strings using current MPFR precision
         */
        void ResetMPConstants();

        /**
         * Check whether a point should be calculated with native arithmetics
         * @param x coordinates
         * @param lProd place to put the calculated worst monom
         * @return whether it should be calculated native
         */
        bool IsArithmeticNative(double *x, double *lProd);

        /**
         * Evaluates the integrand in a single point with MP mode
         * Prepares modified coordinates if needed
         * @param res place to put results to
         * @param lProd calculated worst monom address (it can be modified in case of coordinate modification)
         * @param xLocal coordinates of the point
         */
        void RunSingeMPFRExpr(double *res, double *lProd, double* xLocal);

        /**
         * Evaluates the integrand in sampling points with native mode
         * The sampling points are already loaded into the internal structures
         * @param res place to put results to
         * @param points the number of sampling points that should not exceed batchNumber
         */
        void RunlineNative(double *res, unsigned int points);

        /**
         * Modifies the coordinates if needed in case they are two small even for an MP evaluation
         * @param lProd the address of the precalculated worst product, can be changed
         * @param xLocal the coordinated which can be changed
         */
        void CreateTruncatedX(double *lProd, double *xLocal);

        /**
         * Finally runs the evaluation in MP mode after everything was prepared and loaded into internal structures
         * @param res place to put result
         */
        void RunlineMP(double *res);

    public:
        /**
         * Evaluates the integrand in sampling points
         * @param x the coordinated of all sampling points
         * @param res place to put results to
         * @param points the number of sampling points that should not exceed batchNumber
         */
        void RunExpr(const double *x, double *res, unsigned int points);

        /** Public method to see integral dimension
         * @return dimension of the integral
         */
        unsigned int GetDimension() {
            return theScan->GetDimension();
        }

        /**
         * Builds Runtime evaluation tetrads based on the Scanner data
         */
        void BuildRuntime();

#if defined(GPU) || defined(DOXYGEN_DOCUMENTATION)
        /**
         * Clears memory allocated on a GPU
         */
        void exitCuda();
#endif

        /**
         * Constructor based on a scan class.
         * Sets a pointer to it and resizes Runtime structures to proper length
         * @param scan the scan class
         */
        explicit Runtime(std::unique_ptr<Scanner> scan) : theScan(std::move(scan)) {
            length = theScan->compileTimeTriads.size();
            runtimeOperations.resize(length);
            MPoperands.resize(length);
            nativeOperands.resize(length);
        }
};
