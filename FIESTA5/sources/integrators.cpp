/** @file integrators.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains definition of integrators used to seed sampling points
 */

#include "integrators.h"

#include "../sourcesCommon/util.h"

#include <string.h>

#include <algorithm>
#include <fstream>
#include <sstream>
#include <complex>

const std::string fmtDouble = "%lf"; /**<Format for a double*/
const std::string fmtDoubleExp = "%lE"; /**<Format for a double in exponent-form*/
const std::string fmtLongLong = "%lld"; /**<Format for a long long int*/
const std::string fmtInt = "%d"; /**<Format for an integer*/
const std::string fmtStr = "%s"; /**<Format for a c-based string*/

void Integration::SetParameter(const std::string& integratorName, const std::string& name, const std::string& val) {
    const auto& parameters = integrators_[integratorName].second;
    auto it = std::find_if(begin(parameters), end(parameters), [&] (const auto& parameter) {
        return parameter.name == name;
    });
    if (it == parameters.end()) {
        FIESTA_FAIL(Format("Parameter %s not found for integrator %s", name.c_str(), integratorName.c_str()));
    }
    const auto& parameter = *it;
    if (parameter.fmt == "%lld"){
            FIESTA_VERIFY(
                    sscanf(val.c_str(), "%lld", std::get<long long int*>(parameter.addr)) == 1,
                    "failed to set parameter");
    }
    else if (parameter.fmt == "%lf"){
            FIESTA_VERIFY(
                    sscanf(val.c_str(), "%lf", std::get<double *>(parameter.addr)) == 1,
                    "failed to set parameter");
    }
    else if (parameter.fmt == "%lE"){
            FIESTA_VERIFY(
                    sscanf(val.c_str(), "%lE", std::get<double *>(parameter.addr)) == 1,
                    "failed to set parameter");
    }
    else if (parameter.fmt == "%s"){
            FIESTA_VERIFY(
                    sscanf(val.c_str(), "%s", std::get<char *>(parameter.addr)) == 1,
                    "failed to set parameter");
    }
    else if (parameter.fmt == "%d"){
            FIESTA_VERIFY(
                    sscanf(val.c_str(), "%d", std::get<int *>(parameter.addr)) == 1,
                    "failed to set parameter");
    }
}

std::string Integration::GetAllParameters(const std::string& integratorName) {
    std::stringstream ss;
    ss << '{';
    for (const auto &itr: integrators_[integratorName].second) {
        ss << "{\"" << itr.name << "\",\"";
        if (fmtLongLong == itr.fmt) {
            ss << Format(itr.fmt, *std::get<long long int*>(itr.addr));
        } else if (fmtDouble == itr.fmt) {
            ss << Format(itr.fmt, *std::get<double *>(itr.addr));
        } else if (fmtDoubleExp == itr.fmt) {
            ss << Format(itr.fmt, *std::get<double *>(itr.addr));
        } else if (fmtStr == itr.fmt) {
            ss << Format(itr.fmt, std::get<char *>(itr.addr));
        } else if (fmtInt == itr.fmt) {
            ss << Format(itr.fmt, *std::get<int *>(itr.addr));
        } else {
            FIESTA_FAIL("Unknown format " + itr.fmt);
        }
        ss << "\"},";
    }
    std::string str = ss.str();
    str.erase(str.size() - 1);
    str += "}";
    return str;
}

void Integration::RegisterIntegrator(IntegrationFunction theIntegrator, const std::string& name, const std::vector<IntegratorParameter>& params) {
    integrators_.emplace(name, std::pair{theIntegrator, params});
}

void Integration::SetIntegrator(const std::string& integratorName) {
    if (integrators_.find(integratorName) == integrators_.end()) {
        FIESTA_FAIL(Format("Integrator %s not found", integratorName.c_str()));
    }
    currentIntegrator = integratorName;
}

IntegrationResult Integration::RunIntegrator(const std::string& integratorName) {
    IntegrationResult results;
    integrators_[integratorName].first(&results);
    return results;
}

static void runExprWrapper(int count, const double* point, double* res, void* userdata = nullptr) {
    const auto* curPoint = point;
    auto* curRes = res;
    while (count > Common::batchSize) {
        Integration::globalRuntime->RunExpr(curPoint, curRes, Common::batchSize);
        curPoint += (Integration::globalRuntime->GetDimension()) * Common::batchSize;
        curRes += complexMultiplier * Common::batchSize;
        count -= Common::batchSize;
    }
    Integration::globalRuntime->RunExpr(curPoint, curRes, count);
}

template <typename TRes>
static void runExprWrapperTyped(int count, const double* point, TRes* res, void* userdata = nullptr) {
#ifdef COMPLEX
    static_assert(std::is_same<TRes, std::complex<double>>::value, "expected complex");
#else
    static_assert(std::is_same<TRes, double>::value, "expected double");
#endif

    runExprWrapper(count, point, reinterpret_cast<double*>(res), userdata);
}

/*********** CUBA: ***************************/

static int IntegrandCuba(const int *ndim, const double xx[], const int *ncomp, double ff[], void* userdata, unsigned int *points) {
    runExprWrapper(*points, xx, ff);
    return 0;
}

static int key = 0;
static int key1 = 0;
static int key2 = 0;
static int key3 = 1;

static double epsrel = 1e-5;
static double epsabs = 1e-12;
static long long int mineval = 0;
static long long int maxeval = 50000;
static long long int nstart = 1000;
static long long int nincrease = 500;
static int nnew = 1000;
static double flatness = 25.;
static int maxpass = 5;
static double border = 0.000;
static double maxchisq = 10;
static double mindeviation = .25;

static int seed = 0;
static int rng = 0;

static std::vector<IntegratorParameter> vegasCubaParameters = {
    {"epsrel", fmtDoubleExp, &epsrel},
    {"epsabs", fmtDoubleExp, &epsabs},
    {"mineval", fmtLongLong, &mineval},
    {"maxeval", fmtLongLong, &maxeval},
    {"nstart", fmtLongLong, &nstart},
    {"nincrease", fmtLongLong, &nincrease},
    {"seed", fmtInt, &seed},
    {"rng", fmtInt, &rng},
};

static int vegasCuba(IntegrationResult *result) {
    long long int neval;
    int fail;
    double integral[2], error[2], prob[2];
    cubacores(Common::CPUCores, Common::CPUBatch);
    cubaaccel(Common::GPUCores, Common::GPUBatch);
    llVegas(Integration::globalRuntime->GetDimension(), complexMultiplier, reinterpret_cast<integrand_t>(IntegrandCuba), nullptr, Common::batchSize, epsrel, epsabs,
            (8 * rng),  // flags instead of VERBOSE
            seed,                 // seed
            mineval, maxeval, nstart, nincrease,
            Common::batchSize,  // nbatch
            0,       // gridno
            nullptr,      // statefile
            nullptr,      // cubaspin
            &neval, &fail, integral, error, prob);

    result->resultReal = integral[0];
    result->errorReal = error[0];
    result->resultImaginary = integral[1];
    result->errorImaginary = error[1];

    return (fail > 0);
}

static std::vector<IntegratorParameter> suaveCubaParameters = {
    {"epsrel", fmtDoubleExp, &epsrel},
    {"epsabs", fmtDoubleExp, &epsabs},
    {"mineval", fmtLongLong, &mineval},
    {"maxeval", fmtLongLong, &maxeval},
    {"nnew", fmtInt, &nnew},
    {"flatness", fmtDouble, &flatness},
    {"seed", fmtInt, &seed},
    {"rng", fmtInt, &rng},
};

static int suaveCuba(IntegrationResult *result) {
    int long long neval;
    int nregions, fail;
    double integral[2], error[2], prob[2];
    int dim = Integration::globalRuntime->GetDimension();
    if (dim == 1) dim++;
    cubacores(Common::CPUCores, Common::CPUBatch);
    cubaaccel(Common::GPUCores, Common::GPUBatch);
    llSuave(dim, complexMultiplier, reinterpret_cast<integrand_t>(IntegrandCuba), nullptr, Common::batchSize, epsrel, epsabs, 4 + (8 * rng), seed, mineval,
            maxeval, nnew, flatness, "", nullptr, &nregions, &neval, &fail, integral, error, prob);
    result->resultReal = integral[0];
    result->errorReal = error[0];
    result->resultImaginary = integral[1];
    result->errorImaginary = error[1];
    return (fail > 0);
}

static std::vector<IntegratorParameter> divonneCubaParameters = {
    {"epsrel", fmtDoubleExp, &epsrel},
    {"epsabs", fmtDoubleExp, &epsabs},
    {"mineval", fmtLongLong, &mineval},
    {"maxeval", fmtLongLong, &maxeval},
    {"key1", fmtInt, &key1},
    {"key2", fmtInt, &key2},
    {"key3", fmtInt, &key3},
    {"maxpass", fmtInt, &maxpass},
    {"border", fmtDouble, &border},
    {"maxchisq", fmtDouble, &maxchisq},
    {"mindeviation", fmtDouble, &mindeviation},
    {"seed", fmtInt, &seed},
    {"rng", fmtInt, &rng},
};

static int divonneCuba(IntegrationResult *result) {
    int long long neval;
    int nregions, fail;
    double integral[2], error[2], prob[2];
    int dim = Integration::globalRuntime->GetDimension();
    if (dim == 1) dim++;
    cubacores(Common::CPUCores, Common::CPUBatch);
    cubaaccel(Common::GPUCores, Common::GPUBatch);
    llDivonne(dim, complexMultiplier, reinterpret_cast<integrand_t>(IntegrandCuba), nullptr, Common::batchSize, epsrel, epsabs, 8 * rng, seed, mineval, maxeval, key1,
              key2, key3, maxpass, border, maxchisq, mindeviation, 0, dim, nullptr, 0, nullptr, "", nullptr, &nregions, &neval, &fail, integral, error,
              prob);
    result->resultReal = integral[0];
    result->errorReal = error[0];
    result->resultImaginary = integral[1];
    result->errorImaginary = error[1];
    return (fail > 0);
}

static std::vector<IntegratorParameter> cuhreCubaParameters = {
    {"epsrel", fmtDoubleExp, &epsrel},
    {"epsabs", fmtDoubleExp, &epsabs},
    {"mineval", fmtLongLong, &mineval},
    {"maxeval", fmtLongLong, &maxeval},
    {"key", fmtInt, &key},
};

static int cuhreCuba(IntegrationResult *result) {
    int long long neval;
    int nregions, fail;
    double integral[2], error[2], prob[2];
    int dim = Integration::globalRuntime->GetDimension();
    if (dim == 1) dim++;
    cubacores(Common::CPUCores, Common::CPUBatch);
    cubaaccel(Common::GPUCores, Common::GPUBatch);
    llCuhre(dim, complexMultiplier, reinterpret_cast<integrand_t>(IntegrandCuba), nullptr, Common::batchSize, epsrel, epsabs, 4 + (8 * rng), mineval, maxeval, key,
            "", nullptr, &nregions, &neval, &fail, integral, error, prob);
    result->resultReal = integral[0];
    result->errorReal = error[0];
    result->resultImaginary = integral[1];
    result->errorImaginary = error[1];
    return (fail > 0);
}

/*********** :CUBA ***************************/

#ifdef ENABLE_TT

/*********** Tensor Train: ***************************/

int ttApproximationIterationCount = 4;
char ttQuadratureType[256] = "gauss_kronrod";
int ttNodeCount = 20;
int ttSingularityNodeCount = 0;
double ttSingularityWidth = 0.0;
int ttSimpleNodeCount = 5;
double ttTanhSinhQuadratureStep = 0.1;
int ttPower = 3;

static std::vector<IntegratorParameter> tensorTrainParameters = {
    {"epsrel", fmtDoubleExp, &epsrel},
    {"approximation_iteration_count", fmtInt, &ttApproximationIterationCount},
    {"quadrature_type", fmtStr, ttQuadratureType},
    {"quadrature_node_count", fmtInt, &ttNodeCount},
    {"tanh_sinh_quadrature_step", fmtDoubleExp, &ttTanhSinhQuadratureStep},
    {"quadrature_singularity_width", fmtDoubleExp, &ttSingularityWidth},
    {"quadrature_singularity_node_count", fmtInt, &ttSingularityNodeCount},
    {"quadrature_simple_node_count", fmtInt, &ttSimpleNodeCount},
    {"quadrature_power", fmtInt, &ttPower},
    {"maxeval", fmtLongLong, &maxeval},
};

static int TTIntegrateWrapper(IntegrationResult *result) {
    struct TTIntegratorConfig config;

    if (!strcmp(ttQuadratureType, "trapezoidal")) {
        // it is default
    } else if (!strcmp(ttQuadratureType, "tanh_sinh")) {
        config.type = QT_TANH_SINH;
    } else if (!strcmp(ttQuadratureType, "singularity_mixed")) {
        config.type = QT_SINGULARITY_MIXED;
    } else if (!strcmp(ttQuadratureType, "gauss")) {
        config.type = QT_GAUSS_MIXED;
    } else if (!strcmp(ttQuadratureType, "gauss_kronrod")) {
        config.type = QT_GAUSS_KRONROD;
    } else {
        FIESTA_FAIL(Format("Wrong quadrature type for TT integrator: %s", ttQuadratureType));
    }
    config.approximation_tolerance = epsrel;
    config.approximation_iteration_count = ttApproximationIterationCount;
    config.node_count = ttNodeCount;
    config.tanh_sinh_step = ttTanhSinhQuadratureStep;
    config.singularity_width = ttSingularityWidth;
    config.singularity_node_count = ttSingularityNodeCount;
    config.simple_node_count = ttSimpleNodeCount;
    config.transform = ETransform::TR_POWER;
    config.power_transform_power = ttPower;
    config.evaluation_count_soft_limit = maxeval;
    config.evaluation_count_hard_limit = maxeval * 3;

#ifdef COMPLEX
    std::complex<double> integral, error;
    int ok = TTIntegrateFunPtr<double, std::complex<double>>(
        Integration::globalRuntime->GetDimension(), &config, runExprWrapperTyped, &integral, &error, nullptr);
    result->resultReal = integral.real();
    result->errorReal = error.real();
    result->resultImaginary = integral.imag();
    result->errorImaginary = error.imag();
#else
    double integral, error;
    int ok = TTIntegrateFunPtr<double, double>(
        Integration::globalRuntime->GetDimension(), &config, runExprWrapperTyped, &integral, &error, nullptr);
    result->resultReal = integral;
    result->errorReal = error;
    result->resultImaginary = 0;
    result->errorImaginary = 0;
#endif

    return !ok;
}
/*********** : Tensor Train ***************************/
#endif  // ENABLE_TT

#ifdef ENABLE_QMC

char qmcIntegralTransform[256] = "korobov";

static std::vector<IntegratorParameter> QMCParameters = {
    {"maxeval", fmtLongLong, &maxeval},
    {"epsrel", fmtDoubleExp, &epsrel},
    {"epsabs", fmtDoubleExp, &epsabs},
    {"integralTransform", fmtStr, qmcIntegralTransform},
};

template <typename T, typename D> struct integration_functor {
    unsigned long long int number_of_integration_variables = 0;
    virtual T operator()(D x[]) = 0;
    void operator()(D* x, T* res, unsigned long long count)
    {
        for (unsigned long long i = 0; i != count; ++i) res[i] = this->operator()(x + i*number_of_integration_variables);
    }
};

#ifdef COMPLEX
#include <complex>
#endif

#ifdef COMPLEX
    using FunctionType = std::complex<double>;
#else
    using FunctionType = double;
#endif

static int quasiMonteCarlo(IntegrationResult *result) {
#ifdef COMPLEX
    struct MyFunctor: integration_functor<std::complex<double>, double> {
        std::complex<double> operator()(double x[]) override
        {
            double res[2];
            runExprWrapper(1, x, res);
            return std::complex<double>(res[0], res[1]);
        }
        void operator()(double* x, std::complex<double>* res, unsigned long long count)
        {
            double* resComp = new double[2*count];
            runExprWrapper(count, x, resComp);
            for (unsigned long long i = 0; i != count; ++i) res[i] = std::complex<double>(resComp[2*i], resComp[2*i + 1]);
            delete[] resComp;
        }
    } myFunctor;
#else
    struct MyFunctor: integration_functor<double, double> {
        double operator()(double x[]) override
        {
            double res;
            runExprWrapper(1, x, &res);
            return res;
        }
        void operator()(double* x, double* res, unsigned long long count)
        {
            runExprWrapper(count, x, res);
        }
    } myFunctor;
#endif
    myFunctor.number_of_integration_variables = Integration::globalRuntime->GetDimension();

    integrators::result<FunctionType> resultQMC;

    if (!strcmp(qmcIntegralTransform, "korobov")) {
        integrators::Qmc<FunctionType,double,maxVariables,integrators::transforms::Korobov<3>::type> integrator;
        //integrators::Qmc<FunctionType,double,maxVariables,integrators::transforms::Korobov<3>::type, integrators::fitfunctions::PolySingular::type> integrator;
        integrator.randomgenerator.seed(1);
        integrator.cputhreads = 1;
        integrator.devices = {-1};
        integrator.maxnperpackage = Common::batchSize;
        integrator.batching = true;
        integrator.maxeval = maxeval;
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        resultQMC = integrator.integrate(myFunctor);
    }  else if (!strcmp(qmcIntegralTransform, "sidi")) {
        integrators::Qmc<FunctionType,double,maxVariables,integrators::transforms::Sidi<3>::type> integrator;
        integrator.cputhreads = 1;
        integrator.devices = {-1};
        integrator.randomgenerator.seed(1);
        integrator.maxnperpackage = Common::batchSize;
        integrator.batching = true;
        integrator.maxeval = maxeval;
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        resultQMC = integrator.integrate(myFunctor);
    }  else if (!strcmp(qmcIntegralTransform, "baker")) {
        integrators::Qmc<FunctionType,double,maxVariables,integrators::transforms::Baker::type> integrator;
        integrator.cputhreads = 1;
        integrator.devices = {-1};
        integrator.randomgenerator.seed(1);
        integrator.maxnperpackage = Common::batchSize;
        integrator.batching = true;
        integrator.maxeval = maxeval;
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        resultQMC = integrator.integrate(myFunctor);
    }  else if (!strcmp(qmcIntegralTransform, "none")) {
        integrators::Qmc<FunctionType,double,maxVariables,integrators::transforms::None::type> integrator;
        integrator.cputhreads = 1;
        integrator.devices = {-1};
        integrator.randomgenerator.seed(1);
        integrator.maxnperpackage = Common::batchSize;
        integrator.batching = true;
        integrator.maxeval = maxeval;
        integrator.epsrel = epsrel;
        integrator.epsabs = epsabs;
        resultQMC = integrator.integrate(myFunctor);
    } else {
        FIESTA_FAIL(Format("Wrong integral transform for QMC integrator: \"%.*s\"\n", 256, qmcIntegralTransform));
    }
#ifdef COMPLEX
    result->resultReal = resultQMC.integral.real();
    result->errorReal = resultQMC.error.real();
    result->resultImaginary = resultQMC.integral.imag();
    result->errorImaginary = resultQMC.error.imag();
#else
    result->resultReal = resultQMC.integral;
    result->errorReal = resultQMC.error;
#endif
    return 0;
}
#endif

static std::vector<double> onePoint(40, 0.5);

static int justEvaluate(IntegrationResult *result) {
    double res[8];
    Integration::globalRuntime->RunExpr(onePoint.data(), res, 4);
    result->resultReal = res[0];
    result->errorReal = 0;
    result->resultImaginary = res[1];
    result->errorImaginary = 0;

    return 1;
};

static auto justEvaluateParameters = [] {
    std::vector<IntegratorParameter> params;
    for (int i = 0; i < 10; ++i) {
        params.emplace_back("x" + std::to_string(i + 1), fmtDoubleExp, &onePoint[i]);
    }
    return params;
}();
    
void Integration::InitIntegrators() {
    if (init) {
        return;
    }

    RegisterIntegrator(&vegasCuba, "vegasCuba", vegasCubaParameters);
    RegisterIntegrator(&suaveCuba, "suaveCuba", suaveCubaParameters);
    RegisterIntegrator(&divonneCuba, "divonneCuba", divonneCubaParameters);
    RegisterIntegrator(&cuhreCuba, "cuhreCuba", cuhreCubaParameters);
#ifdef ENABLE_TT
    RegisterIntegrator(&TTIntegrateWrapper, "tensorTrain", tensorTrainParameters);
#endif
#ifdef ENABLE_QMC
    RegisterIntegrator(&quasiMonteCarlo, "quasiMonteCarlo", QMCParameters);
#endif
    RegisterIntegrator(&justEvaluate, "justEvaluate", justEvaluateParameters);

    SetIntegrator("vegasCuba");

    init = true;
}
