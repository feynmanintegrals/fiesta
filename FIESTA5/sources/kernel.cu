/** @file kernel.cu
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains GPU functions evaluating the integrand on a GPU
 */
#include "kernel.h"

#define IF_VALID_THREAD                              \
    int tid = blockIdx.x * blockDim.x + threadIdx.x; \
    if (tid < N)

#ifndef COMPLEX

#define NP_PLUS_GPU(are, aim, bre, bim, cre, cim) are = bre + cre
#define NP_MUL_GPU(are, aim, bre, bim, cre, cim) are = bre * cre
#define NP_MINUS_GPU(are, aim, bre, bim, cre, cim) are = bre - cre
#define NP_DIV_GPU(are, aim, bre, bim, cre, cim) are = bre / cre
#define NP_IPOW_GPU(are, aim, bre, bim, deg) \
    {                                        \
        double x = bre;                       \
        double z;                             \
        int y = deg;                         \
        double u;                             \
        if (y == 2)                          \
            u = x * x;                       \
        else {                               \
            if ((y & 1) != 0)                \
                u = x;                       \
            else                             \
                u = 1.0L;                    \
            z = x;                           \
            y >>= 1;                         \
            while (y) {                      \
                z = z * z;                   \
                if ((y & 1) != 0) u *= z;    \
                y >>= 1;                     \
            }                                \
        }                                    \
        are = u;                             \
    }
#define NP_POW2_GPU(are, aim, bre, bim) (are) = (bre) * (bre)
#define NP_POW3_GPU(are, aim, bre, bim) (are) = (bre) * (bre) * (bre)
#define NP_POW4_GPU(are, aim, bre, bim) \
    {                                   \
        double temp;                     \
        (temp) = (bre) * (bre);         \
        (are) = (temp) * (temp);        \
    }
#define NP_POW5_GPU(are, aim, bre, bim)  \
    {                                    \
        double temp;                      \
        (temp) = (bre) * (bre);          \
        (are) = (temp) * (temp) * (bre); \
    }
#define NP_POW6_GPU(are, aim, bre, bim)   \
    {                                     \
        double temp;                       \
        (temp) = (bre) * (bre);           \
        (are) = (temp) * (temp) * (temp); \
    }
#define NP_CPY_GPU(are, aim, bre, bim) (are) = (bre)
#define NP_NEG_GPU(are, aim, bre, bim) (are) = -(bre)
#define NP_INV_GPU(are, aim, bre, bim) (are) = 1.0 / (bre)
#define NP_LOG_GPU(are, aim, bre, bim) \
    if ((bre) <= 1.0E-100)             \
        (are) = -230.258509299405;     \
    else                               \
        (are) = log(bre);
#define NP_POW_GPU(are, aim, bre, bim, cre, cim) (are) = pow((bre), (cre))

#else

#define NP_PLUS_GPU(are, aim, bre, bim, cre, cim) \
    are = bre + cre;                              \
    aim = bim + cim;
#define NP_MINUS_GPU(are, aim, bre, bim, cre, cim) \
    are = bre - cre;                               \
    aim = bim - cim;
#define NP_MUL_GPU(are, aim, bre, bim, cre, cim) \
    are = (bre * cre) - (bim * cim);             \
    aim = (bim * cre) + (bre * cim);
#define NP_DIV_GPU(are, aim, bre, bim, cre, cim) \
    double denom = (cre * cre) + (cim * cim);     \
    are = ((bre * cre) + (bim * cim)) / denom;   \
    aim = ((bim * cre) - (bre * cim)) / denom;

#define NP_IPOW_GPU(are, aim, bre, bim, deg)  \
    int y = deg;                              \
    ;                                         \
    ;                                         \
    double temp;                               \
    if (y == 2) {                             \
        are = (bre * bre) - (bim * bim);      \
        aim = 2 * (bre * bim);                \
    } else {                                  \
        if ((y & 1) != 0) {                   \
            are = bre;                        \
            aim = bim;                        \
        } else {                              \
            are = 1.0L;                       \
            aim = 0.0L;                       \
        }                                     \
        y >>= 1;                              \
        while (y) {                           \
            temp = bre * bre - bim * bim;     \
            bim = 2 * bre * bim;              \
            bre = temp;                       \
            if ((y & 1) != 0) {               \
                temp = are * bre - aim * bim; \
                aim = are * bim + aim * bre;  \
                are = temp;                   \
            }                                 \
            y >>= 1;                          \
        }                                     \
    }

#define NP_POW2_GPU(are, aim, bre, bim) \
    are = (bre * bre) - (bim * bim);    \
    aim = 2 * (bim * bre);
#define NP_POW3_GPU(are, aim, bre, bim)          \
    are = bre * bre * bre - 3 * bre * bim * bim; \
    aim = 3 * bim * bre * bre - bim * bim * bim;
#define NP_POW4_GPU(are, aim, bre, bim)        \
    {                                          \
        double tre = (bre * bre) - (bim * bim); \
        double tim = 2 * (bim * bre);           \
        are = (tre * tre) - (tim * tim);       \
        aim = 2 * (tre * tim);                 \
    }
#define NP_POW5_GPU(are, aim, bre, bim) \
    { NP_IPOW_GPU(are, aim, bre, bim, 5) }

#define NP_POW6_GPU(are, aim, bre, bim)                  \
    {                                                    \
        double tre = (bre * bre) - (bim * bim);           \
        double tim = 2 * (bim * bre);                     \
        are = (tre * tre * tre) - (3 * tre * tim * tim); \
        aim = (3 * tre * tre * tim) - (tim * tim * tim); \
    }
#define NP_CPY_GPU(are, aim, bre, bim) \
    (are) = (bre);                     \
    (aim) = (bim)
#define NP_NEG_GPU(are, aim, bre, bim) \
    (are) = -(bre);                    \
    (aim) = -(bim)
#define NP_INV_GPU(are, aim, bre, bim)       \
    {                                        \
        double denom = bre * bre + bim * bim; \
        are = bre / denom;                   \
        aim = -bim / denom;                  \
    }

#define NP_LOG_GPU(are, aim, bre, bim)                       \
    ;                                                        \
    ;                                                        \
    if ((bre <= 1.0E-100) && (bre >= 0) && (bim == 0.0)) {   \
        are = -230.258509299405;                             \
        aim = 0;                                             \
    } else if ((bim == 0.0) && (bre > 0)) {                  \
        are = log(bre);                                      \
        aim = 0;                                             \
    } else if ((bim == 0.0) && (bre < 0)) {                  \
        are = log(-bre);                                     \
        aim = -M_PI;                                         \
    } else if ((bim > 0) && (bre <= bim) && (bre >= -bim)) { \
        double denom = bre * bre + bim * bim;                 \
        are = log(denom) / 2;                                \
        aim = M_PI / 2 - atan(bre / bim);                    \
    } else if ((bim < 0) && (bre <= -bim) && (bre >= bim)) { \
        double denom = bre * bre + bim * bim;                 \
        are = log(denom) / 2;                                \
        aim = -M_PI / 2 - atan(bre / bim);                   \
    } else if (bre > 0) {                                    \
        double denom = bre * bre + bim * bim;                 \
        are = log(denom) / 2;                                \
        aim = atan(bim / bre);                               \
    } else if (bim > 0) {                                    \
        double denom = bre * bre + bim * bim;                 \
        are = log(denom) / 2;                                \
        aim = atan(bim / bre) + M_PI;                        \
    } else {                                                 \
        double denom = bre * bre + bim * bim;                 \
        are = log(denom) / 2;                                \
        aim = atan(bim / bre) - M_PI;                        \
    }
#define NP_POW_GPU(are, aim, bre, bim, cre, cim) \
    { are = 1 / (1 - 1.); }
#endif

__global__ void
//__launch_bounds__( cudaBlockSize, CUDA_MIN_BLOCKS)
EVALUATION_KERNEL(unsigned int length, int N, unsigned int batchSizeLocal, double** firstOperandGpuInternal, double** secondOperandGpuInternal,
                  unsigned int* secondOperandIGpuInternal, double** resultGpuInternal, Operation* operationGpuInternal) {
    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    double* indFirst;
    double* indSecond;
    double* indResult;

    double are, bre, cre;

#ifndef COMPLEX
#define SET_OPERAND1 \
    { bre = indFirst[tid]; }
#define SET_OPERAND2 \
    { cre = indSecond[tid]; }
#else
    double aim, bim, cim;
#define SET_OPERAND1                           \
    {                                          \
        bre = indFirst[tid];                  \
        bim = indFirst[tid + batchSizeLocal]; \
    }
#define SET_OPERAND2                            \
    {                                           \
        cre = indSecond[tid];                  \
        cim = indSecond[tid + batchSizeLocal]; \
    }
#endif

    if (tid < N) {
        int i;
        for (i = 0; i != length; ++i) {
            Operation op = operationGpuInternal[i];
            indFirst = firstOperandGpuInternal[i];
            indResult = resultGpuInternal[i];
            SET_OPERAND1

            switch (op) {
                case Operation::PLUS: {
                    indSecond = secondOperandGpuInternal[i];
                    SET_OPERAND2 NP_PLUS_GPU(are, aim, bre, bim, cre, cim);
                    break;
                }
                case Operation::MUL: {
                    indSecond = secondOperandGpuInternal[i];
                    SET_OPERAND2 NP_MUL_GPU(are, aim, bre, bim, cre, cim);
                    break;
                }
                case Operation::MINUS: {
                    indSecond = secondOperandGpuInternal[i];
                    SET_OPERAND2 NP_MINUS_GPU(are, aim, bre, bim, cre, cim);
                    break;
                }
                case Operation::DIV: {
                    indSecond = secondOperandGpuInternal[i];
                    SET_OPERAND2 NP_DIV_GPU(are, aim, bre, bim, cre, cim);
                    break;
                }
                case Operation::POW: {
                    indSecond = secondOperandGpuInternal[i];
                    SET_OPERAND2 NP_POW_GPU(are, aim, bre, bim, cre, cim);
                    break;
                }
                case Operation::IPOW2: {
                    NP_POW2_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::IPOW3: {
                    NP_POW3_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::IPOW4: {
                    NP_POW4_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::IPOW5: {
                    NP_POW5_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::IPOW6: {
                    NP_POW6_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::IPOW: {
                    NP_IPOW_GPU(are, aim, bre, bim, secondOperandIGpuInternal[i]);
                    break;
                }
                case Operation::CPY: {
                    NP_CPY_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::NEG: {
                    NP_NEG_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::INV: {
                    NP_INV_GPU(are, aim, bre, bim);
                    break;
                }
                case Operation::LOG: {
                    NP_LOG_GPU(are, aim, bre, bim);
                    break;
                }
            }
            indResult[tid] = are;
#ifdef COMPLEX
            indResult[tid + batchSizeLocal] = aim;
#endif
        }  // for
    }      // if tid <N
}  // kernel

void RunlineNativeGPU(unsigned int bunch, unsigned int batchSize, unsigned int length, double** firstOperandGPU, double** secondOperandGPU, unsigned int* secondOperandIntegerGPU,
        double** resultGPU, Operation* operationGPU) {
    SAFE_KERNEL_CALL((EVALUATION_KERNEL<<<dim3((bunch - 1) / cudaBlockSize + 1), dim3(cudaBlockSize)>>>(
        length, bunch, batchSize, firstOperandGPU, secondOperandGPU, secondOperandIntegerGPU, resultGPU, operationGPU)));
    return;
}

