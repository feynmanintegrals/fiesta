SetOptions[$Output,PageWidth->Infinity];
Print["Expected result is about (0.3333360421512515 + pm[-3]*1.363577668885818e-05) * (ep)^-3 + (0.8528648535923689 + pm[-2]*0.0002944026713127727) * (ep)^-2 + (5.674130518701094 + pm[-1]*0.0006732965557686275) * (ep)^-1 + (-7.472836714489146e-06 + pm[-1]*6.550099301456141e-05) * I (ep)^-1 + (10.86124520233799 + pm[0]*0.00301525387365561) * (ep)^0 + (0.4054983006259066 + pm[0]*0.0005033102997169528) * I (ep)^0"];
Get["FIESTA5.m"];
SetOptions[FIESTA,
    "NumberOfSubkernels" -> 4,
    "NumberOfLinks" -> 4,
    "ComplexMode" -> True,
    "FixSectors" -> True,
    "SectorSplitting" -> True,
    "SectorSymmetries" -> True,
    "ContourShiftShape" -> 1,
    "ContourShiftCoefficient" -> 1/2,
    "XVar" -> y
];
result = SDEvaluate[
 UF[{p1, p2, p3}, {-p1^2,
   m1^2 - p2^2, -p3^2, -(p1 - p2)^2, -(p2 - p3)^2, -(p1 + q1)^2,
   m1^2 - (p2 + q1)^2, -(p3 + q1)^2, -(p1*p3)}, {q1^2 -> 1,
   m1 -> 1}], {0, 1, 1, 1, 1, 1, 1, 0, 0}, 0,
   DebugParallel -> True
];
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
