SetOptions[$Output,PageWidth->Infinity];
Get["FIESTA5.m"];
SetOptions[FIESTA,
    "NumberOfSubkernels" -> 4,
    "NumberOfLinks" -> 4,
    "ComplexMode" -> True,
    "Integrator" -> "quasiMonteCarlo"
];
result = SDEvaluate[
    UF[
        {p1, p10, p11, p5},
        {
            M2^2 - p1^2, -(-p1 + q1)^2, M1^2 - (-p10 - p11 + q1)^2, -(p10 + p11)^2, -p5^2, M2^2 - (p1 + p5)^2,
            M2^2 - (p1 - p10 + p5)^2, -(-p10 + p5 + q1)^2,-(-p10 - p11 + p5 + q1)^2, -p10^2, -p11^2, -(p1 + p10)^2,
            -(p1 + p11)^2,-(p10 + q1)^2
        },
        {M1 -> 1, M2 -> 10, q1^2 -> 1}
    ],
    {0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0},
    -1
];
WriteString[Streams["stderr"],ToString[result,InputForm]];
WriteString[$Output,"\n"];
