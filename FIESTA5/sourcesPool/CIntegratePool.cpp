/** @file CIntegratePool.cpp
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains implementations of functions for the pool,
 *  a program that takes data from a database and sends it to multiple workers for integration.
 */

#include "CIntegratePoolCommon.h"

#include "../sourcesCommon/error.h"
#include "../sourcesCommon/util.h"
//#include "../sourcesCommon/handler.h"

#include <numeric>
#include <array>

std::atomic<bool> timeToStop{false}; /**<In threads mode used to indicate to working threads that they should stop waiting for new results and exit, in MPI mode stops the guard*/

#if defined(THREADSMODE) == defined(MPIMODE)
#error "Exactly one of THREADSMODE and MPIMODE must be specified"
#endif

#if defined(THREADSMODE) || defined(DOXYGEN_DOCUMENTATION)

/**
 * This function is run inside a thread to make one master and multiple workers
 * It takes jobs from the queue and puts them via pipe to CIntegrate,
 * waits for results, reads them and puts to result queue.
 * @param pipeNumber Number from 0 to number of threads minus 1, each thread uses a separate pipe
 */
void IntegrationThread(int pipeNumber);

#endif

#if defined(MPIMODE) || defined(DOXYGEN_DOCUMENTATION)

/**
 * Tags for MPI communication
 */
enum class MessageMPI {
    integrationRequest, // sent from master to slaves with a request to integrate; contains integrand size, sector and function numbers
    integrand, // sent from master to slaves with integrand data
    timeToStop, // sent from master to slaves with indication to stop working
    parseMode, // sent from master to slaves defining, whether following integrands will be for parsing or for evaluation
    estimateMode, // sent from master to slaves defining, whether we are on the estimation mode
    startSuccess, // sent from slaves to master indicating whether they could start CIntegrate
    answerSize, // sent from slaves to master with size of result
    answer // send from slaves to master with results
};

/**
 * In MPI mode instead of IntegrationThread we have this function that takes requests
 * from the job queue, does not evaluate them here but sends to other MPI processes,
 * then collects them back and sends to result queue picked by Receiver
 * @param badSlaves A vector of 0 or 1 (but we need int due to MPI) of slave status. Element 0 is ignored.
 */
void CommunicateWithSlaves(std::vector<int>& badSlaves);

/**
 * Receives a result from first slave that is ready, pushed in to receiver queue
 * @param mpiSubmittedTasks Maps from submitted tasks to slaves working with them, used to check correctness and remove after recieving
 * @param badSlaves A vector of 0 or 1 (but we neede int due to MPI) of slave status. Element 0 is ignored.
 * @param submittedAll Indication whether all tasks were already submitted and we should take into account messages from guard
 * @return Number of slave that is now free
 */
int ReceiveResultFromMPI(std::unordered_map<std::pair<int, int>, int>& mpiSubmittedTasks, const std::vector<int>& badSlaves, bool submittedAll);

long int maximumEvaluationTimeMPI = 0; /**< Maximum evaluation of an integrand in seconds*/
std::atomic<long int> timeFromLastEvaluationMPI = {}; /**<Time counter used by guard thread, reset when receiving a result*/

/**
 * In MPI mode we start a special thread counting time of evaluations. If a time for an evaluation of an integrand is 10 times longer
 * than a time of a general integrand, we mark the slave working with that integrand as bad and resubmit the job.
 */
void GuardThreadMPI();
#endif

/**
 * Executes a system command and reads result
 * @param cmd The command
 * @return The output of the command
 */
std::string ExecuteCommand(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw FiestaException("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

/**
 * A handler for signals in case of broken pipe (program crashing)
 * @param sig the signal number
 */
void handlerPipe(int sig) {
    printf("One of the CIntegrateMP processes crashed!\n");
    printf("Trying to print the command of the integrand that produces this crash.\n");
    printf("Keep in mind, that with multiple threads it can be not exactly that sector or function number but close to that.\n");
    PoolCommon::PrintToolCommand(PoolCommon::latest.first, PoolCommon::latest.second, "\n");
    signal(sig, SIG_DFL);
    raise(sig);
    abort();
}

/**
 * A handler for signals in case of abort (program crashing)
 * @param sig the signal number
 */
void handlerAbort(int sig) {
    PoolCommon::CloseDatabases();
    printf("Aborting\n");
    signal(sig, SIG_DFL);
    raise(sig);
    abort();
}

/**
 * Initialize MPI. Returns current process MPI id and total number of processors.
 * @param argc pointer to argc
 * @param argv pointer to argv
 * @param[out] myidPtr current process MPI id
 * @param[out] numprocsPtr total number of processors
 * @return void
 */
#if defined(MPIMODE) || defined(DOXYGEN_DOCUMENTATION)
void InitMPI(int* argc, char*** argv, int* myidPtr, int* numprocsPtr) {

    int requested = MPI_THREAD_FUNNELED;
    int provided;
    MPI_Init_thread(argc, argv, requested, &provided);
    MPI_Comm_size(MPI_COMM_WORLD, numprocsPtr);

    auto numprocs = *numprocsPtr;

    if (numprocs <= 1) {
        throw FiestaException("Program needs at least one slave");
    }

    MPI_Comm_rank(MPI_COMM_WORLD, myidPtr);

    auto myid = *myidPtr;

    if (PoolCommon::gpuMode) {
        char NodeName[MPI_MAX_PROCESSOR_NAME];
        int NodeNameLen;
        MPI_Get_processor_name(NodeName, &NodeNameLen);
        NodeNameLen++;
        std::vector<int> NodeNameCountVect(numprocs);
        std::vector<int> NodeNameOffsetVect(numprocs);
        std::vector<char> NodeNameList; // it is not a string intentionally since it can contain multiple \0

        //  Gather node name lengths to master to prepare c-array
        MPI_Gather(&NodeNameLen, 1, MPI_INT, NodeNameCountVect.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);

        if (myid == 0) {
            NodeNameOffsetVect[0] = 0;
            int NodeNameTotalLen = NodeNameCountVect[0];

            //  build offset vector and total char count for all node names
            for (int i = 1; i != numprocs; ++i) {
                NodeNameOffsetVect[i] = NodeNameCountVect[i - 1] + NodeNameOffsetVect[i - 1];
                NodeNameTotalLen += NodeNameCountVect[i];
            }
            //  char-array for all node names
            NodeNameList.resize(NodeNameTotalLen);
        }

        //  Gatherv node names to char-array in master
        MPI_Gatherv(NodeName, NodeNameLen, MPI_CHAR, NodeNameList.data(), NodeNameCountVect.data(), NodeNameOffsetVect.data(), MPI_CHAR, 0, MPI_COMM_WORLD);

        std::vector<int> GPUCoreList(numprocs);
        std::vector<int> GPUMemoryPartList(numprocs, 1);
        // they will only be used at master, but need to be everywhere due to MPI syntax

        if (myid == 0) {
            std::unordered_map<std::string, std::vector<int>> sameNodeNameNumbers;
            std::vector<int> proc_number;  // in_node
            proc_number.reserve(numprocs);
            proc_number.push_back(0);  // for master

            std::string s;

            GPUCoreList[0] = -1;  // the master is not using the GPU anyway
            GPUMemoryPartList[0] = 1;

            // we count equal nodes
            for (int i = 1; i != numprocs; ++i) {
                s = std::string(NodeNameList.data() + NodeNameOffsetVect[i]);
                auto itr = sameNodeNameNumbers.find(s);
                if (itr == sameNodeNameNumbers.end()) {
                    sameNodeNameNumbers.emplace(s, std::vector<int>(1, i));
                } else {
                    itr->second.push_back(i);
                }
            }

            // now setting what GPU is used where
            for (const auto& member : sameNodeNameNumbers) {
                size_t realGPUThreadsPerNode = PoolCommon::GPUThreadsPerNode;
                if (realGPUThreadsPerNode > member.second.size()) {
                    realGPUThreadsPerNode = member.second.size();
                    // cannot have more GPU threads than threads;
                }
                for (size_t threadIndex = 0; threadIndex != member.second.size(); ++threadIndex) {
                    if (threadIndex < realGPUThreadsPerNode) {
                        GPUCoreList[member.second[threadIndex]] = threadIndex % PoolCommon::GPUPerNode;
                        GPUMemoryPartList[member.second[threadIndex]] = realGPUThreadsPerNode / PoolCommon::GPUPerNode;
                        if ((threadIndex % PoolCommon::GPUPerNode) < (realGPUThreadsPerNode % PoolCommon::GPUPerNode)) {
                            GPUMemoryPartList[member.second[threadIndex]]++;
                            // for example, we have GPUPerNode = 3 and realGPUThreadsPerNode = 5. Then GPUMemoryPart will be 2 2 1 2 2 (depending on threadIndex)
                        }
                    } else {
                        // switching off GPU
                        GPUCoreList[member.second[threadIndex]] = -1;
                        GPUMemoryPartList[member.second[threadIndex]] = 1;
                    }
                }
            }
        }
        MPI_Scatter(GPUCoreList.data(), 1, MPI_INT, &PoolCommon::GPUCore, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Scatter(GPUMemoryPartList.data(), 1, MPI_INT, &PoolCommon::GPUMemoryPart, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }
}
#endif

/**
 * Go through all the integration process after databases are open and parameters are read
 * @param myid current process MPI id
 * @param numprocs total number of processes
 * @param processName argv[0]
 * @return void
 */
void RunAllIntegration(int myid, int numprocs, char* processName) {
    if (PoolCommon::printIntegrationCommand) {
        if (myid == 0) {
            std::cout << "Command: " << PoolCommon::integrationCommand << std::endl;
        }
    }
    if (PoolCommon::testMode) {
        if (myid == 0) {
            PoolCommon::RunIntegrationTest();
        }
        return;
    }

    if (myid == 0) {
        PoolCommon::OpenDatabases();
    }

#ifdef MPIMODE
    PoolCommon::threadsNumber = 1;
    // for MPI slaves start one CIntegrate each
    if (myid != 0) {
        // not starting for master in MPI
        int startBad = 0;
        try {
            PoolCommon::StartCIntegrate(/* count */ 1);
        } catch (const std::exception&) {
            startBad = 1;
        }

        MPI_Ssend(&startBad, 1, MPI_INT, 0, static_cast<int>(MessageMPI::startSuccess), MPI_COMM_WORLD);
        if (startBad) {
            return;
        }
    }
    std::vector<int> badSlaves;
    // we receive answers from the slaves confirming initialization
    if (myid == 0) {
        badSlaves.resize(numprocs, 0);
        for (int rank = 1; rank < numprocs; ++rank) {
            MPI_Status status;
            MPI_Recv(&badSlaves[rank], 1, MPI_INT, rank, static_cast<int>(MessageMPI::startSuccess), MPI_COMM_WORLD, &status);
        }
    }
#endif

#ifdef THREADSMODE
    PoolCommon::StartCIntegrate(PoolCommon::threadsNumber);
    pid_t guardPid = PoolCommon::StartGuard(processName);
    if (!guardPid) {
        for (int threadIndex = 0; threadIndex != PoolCommon::threadsNumber; ++threadIndex) {
            kill(PoolCommon::pidCIntegrate[threadIndex], SIGKILL);
            waitpid(PoolCommon::pidCIntegrate[threadIndex], nullptr, 0);
        }
        throw FiestaException("Failed to spawn guard process");
    }
#endif

    if (PoolCommon::direct && !PoolCommon::debug) {
#ifdef MPIMODE
        if (myid == 0) {
            std::cout << "CIntegratePoolMPI 5.0" << std::endl;
            std::cout << "Slaves: " << numprocs - 1 << std::endl;
            int realslaves = numprocs - 1;
            for (int rank = 1; rank < numprocs; ++rank) {
                realslaves -= badSlaves[rank];
            }
            if (realslaves == 0) {
                throw FiestaException("All slaves were unable to initialize MPI");
            }
            std::cout << "Working slaves: " << realslaves << std::endl;
        }
#else
        std::cout << "CIntegratePool 4.4.dev" << std::endl;
        std::cout << "Threads: " << PoolCommon::threadsNumber << std::endl;
#endif
    }

    if (PoolCommon::direct) {
        std::string FIESTA_folder;
        char current[PATH_MAX];
        if (!getcwd(current, PATH_MAX)) {
            throw FiestaException("Can't get current dir name");
        }
        std::string scurrent(current);
        std::string srun(processName);
#ifdef MPIMODE
        srun = srun.substr(0, srun.length() - strlen("bin/CIntegratePoolMPI"));
#else
        srun = srun.substr(0, srun.length() - strlen("bin/CIntegratePool"));
#endif

        if (srun[0] == '/') {  // running with full path
            FIESTA_folder = srun;
        } else {  // relative path, using current dir
            FIESTA_folder = scurrent + "/" + srun;
        }

        if (myid == 0) {
            std::cout << "Path: " << FIESTA_folder << std::endl;
            auto sysCommand = Format("cd %s && git log --oneline -1 2>/dev/null", FIESTA_folder.c_str());
            std::string version = ExecuteCommand(sysCommand.c_str());
            if (version == "") {
                std::cout << "Cannot get version, git not available." << std::endl;
            } else {
                std::cout << "Version: " << version;
#ifdef ENABLE_TT
                sysCommand = Format("cd %s/%s && git log --oneline -1 2>/dev/null", FIESTA_folder.c_str(), "extra/c-tt-library/");
                std::string ttversion = ExecuteCommand(sysCommand.c_str());
                std::cout << "TT library version: " << ttversion;
#endif
            }
        }
    }

#ifdef THREADSMODE
    timeToStop = false;
    std::vector<std::thread> threads(PoolCommon::threadsNumber);
    for (int i = 0; i != PoolCommon::threadsNumber; i++) {
        threads[i] = std::thread(IntegrationThread, i);
    }
#endif

    if (myid == 0) {  // in threads mode it is always true
        for (const auto& task : PoolCommon::tasks) {
            PoolCommon::currentTask = task;
            std::string key = PoolCommon::currentTask + "-Exact";
            auto value = PoolCommon::inputDatabase.Get(key);

            if (PoolCommon::direct && (PoolCommon::resultsMode != SaveMode::noDatabase)) {
                PoolCommon::outputDatabase.Set(key, value);
            }

            if (value == "True") {
                if (PoolCommon::direct && (PoolCommon::resultsMode != SaveMode::noDatabase)) {
                    key = PoolCommon::currentTask + "-ExactValue";
                    value = PoolCommon::inputDatabase.Get(key);
                    PoolCommon::outputDatabase.Set(key, value);
                    std::cout << "Exact value for part " << PoolCommon::currentTask << " : " << value << std::endl;
                    key = PoolCommon::currentTask + "-ExternalExponent";
                    value = PoolCommon::inputDatabase.Get(key);
                    PoolCommon::outputDatabase.Set(key, value);
                }
                continue;
            }

            if (PoolCommon::direct && (PoolCommon::tasks.size() > 1)) {
                std::cout << "PERFORMING TASK " << task << std::endl;
            }

            PoolCommon::PrepareTaskIntegration();

            PoolCommon::currentOnlyParse = PoolCommon::preparse;
            while (true) {
#ifdef MPIMODE
                // giving PoolCommon::currentOnlyParse value to slaves
                long long onlyParseMPI = PoolCommon::currentOnlyParse;
                for (int rank = 1; rank < numprocs; ++rank) {
                    if (!badSlaves[rank]) {
                        MPI_Ssend(&onlyParseMPI, 1, MPI_LONG_LONG, rank, static_cast<int>(MessageMPI::parseMode), MPI_COMM_WORLD);
                    }
                }
#endif
                auto time_startA = std::chrono::steady_clock::now();
                if (PoolCommon::direct && PoolCommon::currentOnlyParse) {
                    std::cout << "Parse check";
                    std::cout.flush();
                }
                for (const auto& prefix : PoolCommon::prefixes) {
                    PoolCommon::currentPrefix = prefix;
                    PoolCommon::currentOnlyEstimate = PoolCommon::balanceSamplingPoints;
#ifdef MPIMODE
                    // giving PoolCommon::currentOnlyEstimate value to slaves
                    long long onlyEstimateMPI = PoolCommon::currentOnlyEstimate;
                    for (int rank = 1; rank < numprocs; ++rank) {
                        if (!badSlaves[rank]) {
                            MPI_Ssend(&onlyEstimateMPI, 1, MPI_LONG_LONG, rank, static_cast<int>(MessageMPI::estimateMode), MPI_COMM_WORLD);
                        }
                    }
#endif
                    PoolCommon::estimatedIntegrals.clear();
                    while (true) {
                        PoolCommon::PreparePrefixIntegration();
#ifdef MPIMODE
                        CommunicateWithSlaves(badSlaves);
#endif
                        PoolCommon::FinishPrefixIntegration();
                        if (PoolCommon::currentOnlyEstimate) {
                            PoolCommon::currentOnlyEstimate = false;
                            double maxValue = std::accumulate(PoolCommon::estimatedIntegrals.begin(), PoolCommon::estimatedIntegrals.end(), 0.,
                                [](double m, const auto& member){return std::max(m, member.second);}
                            );
                            if (maxValue > 0.) {
                                for (auto& member : PoolCommon::estimatedIntegrals) {
                                    member.second /= maxValue;
                                }
                            }
                        } else {
                            break;
                        }
                    }

                }  // prefix
                auto time_stopA = std::chrono::steady_clock::now();
                if (PoolCommon::direct && PoolCommon::currentOnlyParse) {
                    std::cout << ".........." << std::chrono::duration<double>(time_stopA - time_startA).count() << " seconds" << std::endl;
                }
                if (!PoolCommon::currentOnlyParse) {
                    break;
                }
                PoolCommon::currentOnlyParse = false;
            }
        }  // task

#ifdef MPIMODE
        /* Tell all the slaves to exit. */
        long long int size = 0;
        for (int rank = 1; rank < numprocs; ++rank) {
            if (!badSlaves[rank]) {
                MPI_Ssend(&size, 1, MPI_LONG_LONG, rank, static_cast<int>(MessageMPI::timeToStop), MPI_COMM_WORLD);
            }
        }
#endif
    }

#ifdef MPIMODE
    if (myid > 0) {
        // that's a slave working
        MPI_Status status;
        char result[1024];
        std::vector<char> integrand; // it is not a string due to MPI comminucation 

        while (1) {
            long long int size[5];  // second element is task number
            // last element is time to be sent back
            MPI_Recv(size, 4, MPI_LONG_LONG, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            if (status.MPI_TAG == static_cast<int>(MessageMPI::parseMode)) {
                PoolCommon::currentOnlyParse = size[0];
                continue;
            }  // set PoolCommon::currentOnlyParse

            if (status.MPI_TAG == static_cast<int>(MessageMPI::estimateMode)) {
                PoolCommon::currentOnlyEstimate = size[0];
                continue;
            }  // set PoolCommon::currentOnlyEstimate

            if (status.MPI_TAG == static_cast<int>(MessageMPI::timeToStop)) {
                break;  // time to stop
            }

            if (size[0] + 1 > static_cast<long>(integrand.size())) {
                integrand.resize(size[0] + 1);
            }
            MPI_Recv(integrand.data(), size[0], MPI_CHAR, 0, static_cast<int>(MessageMPI::integrand), MPI_COMM_WORLD, &status);
            integrand[size[0]] = '\0';

            auto time_startA = std::chrono::steady_clock::now();
            try {
                size[4] = PoolCommon::RunIntegration(0, integrand.data(), size[3], result);
            } catch (const std::exception& exception) {
                std::strncpy(result, exception.what(), sizeof(result) - 1);
            }
            auto time_stopA = std::chrono::steady_clock::now();

            size[0] = strlen(result);
            size[3] = std::chrono::duration_cast<std::chrono::seconds>(time_stopA - time_startA).count();

            MPI_Send(size, 5, MPI_LONG_LONG, 0, static_cast<int>(MessageMPI::answerSize), MPI_COMM_WORLD);
            MPI_Send(&result, size[0], MPI_CHAR, 0, static_cast<int>(MessageMPI::answer), MPI_COMM_WORLD);
        }
    }
#endif

#ifdef THREADSMODE
    timeToStop = true;
    PoolCommon::job.notify_all();
    for (int i = 0; i != PoolCommon::threadsNumber; i++) {
        threads[i].join();
    }
    kill(guardPid, SIGKILL); // normal exit. kill the guard
    PoolCommon::CloseDatabases();
    PoolCommon::StopCIntegrate(PoolCommon::threadsNumber);
#endif
}

/**
 * Main function of the program.
 * Uses parameters, does not read from stdin
 * @param argc number of arguments
 * @param argv the arguments
 * @return error code
 */
int main(int argc, char* argv[]) {
    signal(SIGPIPE, handlerPipe);
    signal(SIGABRT, handlerAbort);

    //AttachHandler();

    PoolCommon::ParseArgcArgv(argc, argv);

    if (PoolCommon::inputDatabaseName == "") {
        return 0;
    }

    auto timeStart = std::chrono::steady_clock::now();

#ifdef MPIMODE
    int myid, numprocs;
    InitMPI(&argc, &argv, &myid, &numprocs);
#endif

    bool hadError = false;

    try {
#ifdef MPIMODE
        RunAllIntegration(myid, numprocs, argv[0]);
#else
        RunAllIntegration(0, 1, argv[0]);
#endif
    } catch (const std::exception& exception) {
        hadError = true;
#ifdef MPIMODE
        if (myid == 0) {
#endif
            std::cout << "Error happened during integration: " << exception.what() << std::endl;
#ifdef MPIMODE
        }
#endif
    }

#ifdef MPIMODE
    if (myid == 0) {
        //master
        PoolCommon::CloseDatabases();
    } else {
        //slave
        PoolCommon::StopCIntegrate(/* count */ 1);
    }
    MPI_Finalize();
#endif

    auto timeStop = std::chrono::steady_clock::now();
    if ((PoolCommon::direct) && (!PoolCommon::debug) && ((PoolCommon::tasks.size() > 1) || (PoolCommon::prefixes.size() > 1))) {
        auto seconds = std::chrono::duration<double>(timeStop - timeStart).count();
        std::cout << "Total time: " <<  seconds << " seconds" << std::endl;
    }

    if (hadError) {
        return 1;
    } else {
        return 0;
    }
}

#ifdef THREADSMODE

void IntegrationThread(int pipeNumber) {
    while (true) {
        std::unique_lock lock(PoolCommon::queueMutex);
        PoolCommon::job.wait(lock,
            [] { return (PoolCommon::stopSubmitter || timeToStop || (!PoolCommon::jobQueue.empty())); }
        );
        if (PoolCommon::stopSubmitter) {
            break;
        }
        if (timeToStop) {
            break;
        }
        auto task = PoolCommon::jobQueue.front();
        PoolCommon::latest = {task.sector, task.function};
        PoolCommon::jobQueue.pop();
        lock.unlock();
        PoolCommon::jobQueueFilled.notify_one();

        char result[1024];
        try {
            long long currentMaxeval = 0;
            if (!PoolCommon::currentOnlyEstimate && PoolCommon::balanceSamplingPoints) {
                currentMaxeval = PoolCommon::balancedMaxeval(task.sector, task.function);
            }
            PoolCommon::RunIntegration(pipeNumber, task.data.c_str(), currentMaxeval, result); // communicates via pipe
        } catch (const std::exception& exception) {
            std::strncpy(result, exception.what(), sizeof(result) - 1);
        }
        std::unique_lock rlock(PoolCommon::rqueueMutex);
        PoolCommon::rjobQueueFilled.wait(rlock,
            []{ return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); }
        );
        PoolCommon::rjobQueue.emplace(task.sector, task.function, std::string(result));
        PoolCommon::rjob.notify_one();
    }
}

#else

void GuardThreadMPI() {
    struct timespec reqtime;
    reqtime.tv_sec = 1;
    reqtime.tv_nsec = 0;
    while (true) {
        do {
            nanosleep(&reqtime, nullptr);
            if (timeToStop) {
                return;
            }
            timeFromLastEvaluationMPI++;
            fflush(stdout);
        } while ((maximumEvaluationTimeMPI == 0) || (timeFromLastEvaluationMPI < maximumEvaluationTimeMPI * 10));

        fflush(stdout);

        long long int size[4] = {maximumEvaluationTimeMPI, 0, 0, 0};
        MPI_Request request;
        MPI_Isend(size, 4, MPI_LONG_LONG, 0, static_cast<int>(MessageMPI::answerSize), MPI_COMM_WORLD, &request);
        timeFromLastEvaluationMPI = 0;
    }
}

int ReceiveResultFromMPI(std::unordered_map<std::pair<int, int>, int>& mpiSubmittedTasks, const std::vector<int>& badSlaves, bool submittedAll) {
    MPI_Status status;
    long long int size[5];
    std::vector<char> result;

    while (true) {
        MPI_Recv(size, 5, MPI_LONG_LONG, MPI_ANY_SOURCE, static_cast<int>(MessageMPI::answerSize), MPI_COMM_WORLD, &status);

        if (status.MPI_SOURCE == 0) {
            if (submittedAll) {
                return 0;  // all jobs were already submitted; now we can call the slow slaves bad and resubmit them
            } else {
                std::cout << "Ignoring answer from guard (" << maximumEvaluationTimeMPI << "," << size[0] << ")" << std::endl;
                continue;  // we did not submit all jobs yet! have to wait longer!
            }
        }

        timeFromLastEvaluationMPI = 0;

        if ((maximumEvaluationTimeMPI == 0) || (size[3] > maximumEvaluationTimeMPI)) {
            if (size[3] == 0) {
                maximumEvaluationTimeMPI = 1;
            } else {
                maximumEvaluationTimeMPI = size[3];
            }
        }
        PoolCommon::termsCount++;
        PoolCommon::pointsCount += size[4];

        result.resize(size[0] + 1);
        MPI_Recv(result.data(), size[0], MPI_CHAR, status.MPI_SOURCE, static_cast<int>(MessageMPI::answer), MPI_COMM_WORLD, &status);

        if (badSlaves[status.MPI_SOURCE]) {
            std::cout << "(ignoring late result from slave " << status.MPI_SOURCE << ")";
        } else {
            break;
        }
    }

    result[size[0]] = '\0';

    if (PoolCommon::stopReceiver) {
        return status.MPI_SOURCE;
    }
    // no need to push them into queue, we are exiting anyway

    auto itr = mpiSubmittedTasks.find(std::pair{size[1], size[2]});
    if (itr == mpiSubmittedTasks.end()) {
        throw FiestaException("Received a result from MPI that we were not waiting for");
    }
    mpiSubmittedTasks.erase(itr);

    // puts result into queue for receiver
    std::unique_lock rlock(PoolCommon::rqueueMutex);
    PoolCommon::rjobQueueFilled.wait(rlock,
        [] { return (PoolCommon::rjobQueue.size() < PoolCommon::queueSize); }
    );
    PoolCommon::rjobQueue.emplace(size[1], size[2], std::string(result.data()));
    PoolCommon::rjob.notify_one();
    return status.MPI_SOURCE;
}

void CommunicateWithSlaves(std::vector<int>& badSlaves) {

    int numprocs = badSlaves.size();

    bool submittedAll = false;

    std::thread guardThread = std::thread(GuardThreadMPI);
    auto guard = Finally([&] {
        timeToStop = true;
        guardThread.join();
    });

    int used_slaves = 0;
    int count_slaves = 0;

    // in continue mode we do not restart currentPrefix integration
    if ((PoolCommon::resultsMode != SaveMode::none) && (PoolCommon::resultsMode != SaveMode::noDatabase)) {
        std::string value;
        if (PoolCommon::outputDatabase.TryGet(PoolCommon::currentTask + "-" + PoolCommon::currentPrefix + "-R", &value)) {
            if (value.substr(0, 6) != "{Print") {
                return;
            }
        }
    }

    // this map stores correspondence from tasks to slaves working with them
    std::unordered_map<std::pair<int, int>, int> mpiSubmittedTasks;

    // in this cycle we take jobs from the Queue of Submitter, push first numprocs to MPI,
    // then start waiting fo free slaves to resubmit
    for (int i = PoolCommon::firstSector; i <= PoolCommon::lastSector; i++) {
        std::string key = PoolCommon::currentTask + "-" + PoolCommon::currentPrefix + "-" + std::to_string(i);
        auto value = PoolCommon::inputDatabase.Get(key);
        int sectorFunctions = std::stoi(value);

        if ((!PoolCommon::separateTerms) && (sectorFunctions != 0)) sectorFunctions = 1;

        for (int j = 1; j <= sectorFunctions; j++) {
            // taking task from queue
            std::unique_lock lock(PoolCommon::queueMutex);
            PoolCommon::job.wait(lock,
                [] { return (PoolCommon::stopSubmitter || (!PoolCommon::jobQueue.empty())); }
            );
            if (PoolCommon::stopSubmitter) {
                break;
            }
            auto task = PoolCommon::jobQueue.front();
            PoolCommon::jobQueue.pop();
            lock.unlock();
            PoolCommon::jobQueueFilled.notify_one();

            int free_slave;
            count_slaves++;
            while ((count_slaves < numprocs) && (badSlaves[count_slaves])) {
                count_slaves++;
            }

            if (count_slaves < numprocs) {
                // we still did not use all slaves
                used_slaves++;
                free_slave = count_slaves;
            } else {
                // here we wait for an answer from one of the slaves
                free_slave = ReceiveResultFromMPI(mpiSubmittedTasks, badSlaves, submittedAll);
            }

            // now we can submit the new job
            long long int size[4] = {static_cast<int>(task.data.size()), task.sector, task.function, PoolCommon::balancedMaxeval(task.sector, task.function)};
            mpiSubmittedTasks.emplace(std::pair{task.sector, task.function}, free_slave);
            // task sent to slaves
            MPI_Send(size, 4, MPI_LONG_LONG, free_slave, static_cast<int>(MessageMPI::integrationRequest), MPI_COMM_WORLD);
            MPI_Send(const_cast<char*>(task.data.c_str()), size[0], MPI_CHAR, free_slave, static_cast<int>(MessageMPI::integrand), MPI_COMM_WORLD);
        }
    }

    /* There's no more work to be done, so receive all the remaining results from the slaves. */
    // in case of hadError we also need to collect results bask from slaves

    submittedAll = true;

    for (int rank = 0; rank < used_slaves; ++rank) {
        int free_slave = ReceiveResultFromMPI(mpiSubmittedTasks, badSlaves, submittedAll);
        if (free_slave == 0) {
            // this means that we encounter a timeout; we need to resubmit all jobs
            std::set<std::pair<int, int>> ptasks;
            for (auto const& [task, slave_number] : mpiSubmittedTasks) {
                badSlaves[slave_number] = 1;
                ptasks.insert(task);
            }
            std::cout << "(slow slaves (" << mpiSubmittedTasks.size() << "), resubmitting)";
            mpiSubmittedTasks.clear();  // we will be resubmitting
            rank--;                       // we did not actually receive a result this time
            auto pitr = ptasks.begin();
            for (free_slave = 1; free_slave != numprocs; ++free_slave) {
                if (pitr == ptasks.end()) {
                    break;                                           // we submitted all
                }
                if (badSlaves[free_slave]) {
                    continue;                                       // this one is bad
                }
                std::string temp = PoolCommon::GenerateIntegrand(pitr->first, pitr->second);  // forming the string again
                long long int size[4] = {static_cast<int>(temp.size()), pitr->first, pitr->second, PoolCommon::balancedMaxeval(pitr->first, pitr->second)};
                mpiSubmittedTasks.emplace(*pitr, free_slave);
                MPI_Send(size, 4, MPI_LONG_LONG, free_slave, static_cast<int>(MessageMPI::integrationRequest), MPI_COMM_WORLD);
                MPI_Send(const_cast<char*>(temp.c_str()), size[0], MPI_CHAR, free_slave, static_cast<int>(MessageMPI::integrand), MPI_COMM_WORLD);
                ++pitr;
            }
            if (pitr != ptasks.end()) {
                throw FiestaException("Too many slaves died, could not resubmit all");
            }
        }
    }
}

#endif
