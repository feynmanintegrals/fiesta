/** @file CIntegratePoolCommon.h
 *
 *  This file is a part of the FIESTA package
 *
 *  This file contains the definitions of global variables and functions for the pool,
 *  a program that takes data from a database and sends it to multiple workers for integration.
 *  This part is common for threads and mpi versions.
 */

#pragma once

#ifdef MPIMODE
// save diagnostic state
#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#include <mpi.h>
// turn the warnings back on
//#pragma GCC diagnostic pop
#endif

#include <kchashdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <unordered_map>
#include <mutex>
#include <set>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

namespace std {
    /**
     * Defined hash function for a pair of ints
     */
    template<>
    struct hash<pair<int, int>> {
        /**
         * The hash function for a a pair of ints itself
         * @param p pair of ints
         * @return the hash value
         */
        inline size_t operator()(const pair<int, int>& p) const {
            return p.first | p.second;
        }
    };
};

/**
 * Mode whether we save all results or continue with intermediate results
 */
enum class SaveMode {
    none,
    current,
    all,
    noDatabase
};

/**
 * Mode for point balancing, what is measured
 */
enum class BalanceMode {
    realValue,
    realError,
    imaginaryValue,
    imaginaryError,
    normValue,
    normError
};

/**
 * A task used in integration queues
 */
class IntegrationTask {
    public:
        int sector; /**<Sector number*/
        int function; /**<Function number in case of split sectors, minus number of functions otherwise*/
        std::string data; /**<Integrant or resultt:qa*/

        /**
         * Constructor based on sector, function and data
         * @param sector sector number
         * @param function function number
         * @param data integrand or result data
         */
        IntegrationTask(int sector, int function, std::string data) : sector(sector), function(function), data(std::move(data)) {};
};

/**
 * Class wrapping kyotocabinet HashDB
 */
class Database {
    public:
        /**
         * Tune the database parameters
         * @param bucketCount
         * @param compressor
         * @return void
         */
        void Tune(uint64_t bucketCount, kyotocabinet::Compressor* compressor);


        /**
         * Open the database
         * @param path
         * @param mode
         * @return void
         */
        void Open(const std::string& path, uint32_t mode);


        /**
         * Retrieve the value of a record. Throw exception if it is missing.
         * @param key
         * @return value
         */
        std::string Get(const std::string& key);

        /**
         * Try to retrieve the value of a record.
         * @param key
         * @param[out] value pointer to the value
         * @return whether the record was found
         */
        bool TryGet(const std::string& key, std::string* value);

        /**
         * Retrieve the value of a record. Throw exception if it is missing.
         * @param key
         * @param[out] value pointer to the value
         * @return void
         */
        void Get(const std::string& key, std::string* value);

        /**
         * Set record
         * @param key
         * @param value
         * @return void
         */
        void Set(const std::string& key, const std::string& value);

        /**
         * Add record if the key is missing, do nothing otherwise.
         * @param key
         * @param value
         * @return whether the key was added
         */
        bool Add(const std::string& key, const std::string& value);

        /**
         * Remove the entry with the given key
         * @param key key to remove
         * @return whether the key was found.
         */
        bool Remove(const std::string& key);

        /**
         * Closes the database
         * @return void
         */
        void Close();

        /**
         * Destructor closes the database if necessary.
         */
        ~Database();

    private:
        kyotocabinet::HashDB db_; /**<Underlying database object */
        bool open_ = false; /**<Is the database open */
};

/**
 * Class with static global mebmers common for all pool code
 */
class PoolCommon {
    public:
        /**
         * Start a console program and prepare pipes for data exchange
         * @param inputPipe Pointer to a pipe where we will write data coming to program stdin
         * @param outputPipe Pointer to a pipe where we will read data coming from program stdout
         * @param name Name of the program (with full or relative path)
         * @return program process id
         */
        static pid_t OpenProgram(FILE** inputPipe, FILE** outputPipe, const std::string& name);

        /**
         * Inits parameters of the CIntegrate program by writing them to program stdin
         * @param inputPipe Pipe that is stdin for the program
         * @param outputPipe Pipe that is stdin for the program
         * @return void
         */
        static void InitCIntegrateParameters(FILE* inputPipe, FILE* outputPipe);

        /**
         * Starts a number of CIntegrate programs,
         * sets proper GPU parameters for each CIntegrate,
         * inits all parameters;
         * Calls OpenProgram and InitCIntegrateParameters
         * @param count Number of CIntegrate to be started
         * @return void
         */
        static void StartCIntegrate(int count);

        /**
         * Stops started CIntegrate proprams by sending Exit to them and joining processes
         * @param count Number of CIntegrate to be stopped
         * @return void
         */
        static void StopCIntegrate(int count);

        /**
         * Starts a special process that will forever wait on reading from input pipe,
         * ignore the signal coming from pipe break and at the moment the pipe is broken (CIntegratePool finished)
         * kills the CIntegrate processes using their pids.
         * This is usefull for not leaving some of the CIntegrate processes working in case of crashes.
         * @param name The pointer to argv[0] so that the new process can change its name in the process list to Guard
         * @return Guard process id on success, 0 on fail
         */
        static pid_t StartGuard(char* name);

        /**
         * Parse input of the program
         * @param argc Number of arguments
         * @param argv Array of arguments
         * @return void
         */
        static void ParseArgcArgv(int argc, char* argv[]);

        /**
         * Opens input and output (in case of direct) databases
         * @return void
         */
        static void OpenDatabases();

        /**
         * Closes input and output (if needed) databases
         * @return void
         */
        static void CloseDatabases();

        /**
         * Function run in a separate thread
         * Submitter reads from input database and fills queue
         *  - in threads mode IntegrateThread catches the queue
         *  - in MPI mode it should be started by the master and
         *    the main thread reads out from the queue and sumbits via MPI
         * @return void
         */
        static void Submitter();

        /**
         * Helper function for Submitter()
         * @return void
         */
        static void DoSubmit();

        /** 
         * Function run in a separate thread
         * Receiver reads from queue with results
         *  - in threads mode they come from IntegrateThread
         *  - in MPI mode the master-thread fills the results queue after MPI exchange
         * @return void
         */
        static void Receiver();

        /**
         * Helper function for Receiver()
         * @return void
         */
        static void DoReceive();

        /**
         * Read out information related to currentTask (for most types of jobs it is a single task)
         * @return void
         */
        static void PrepareTaskIntegration();

        /**
         * Read out from database information related to a currentPrefix,
         * start sumbitter and receiver
         * @return void
         */
        static void PreparePrefixIntegration();

        /**
         * Clean up after integrating a currentPrefix
         * @return void
         */
        static void FinishPrefixIntegration();

        /**
         * Forms an integrand ready to be sent to CIntegrate
         * Used global currentTask and currentPrefix
         * Gets information from input database
         * @param sector the number of the integrand sector
         * @param function if positive, the function to be taken from that sector, if negative, minus the number of functions in the sector
         * @return integrand string
         */
        static std::string GenerateIntegrand(int sector, int function);

        /**
         * In case of balancing evaluate the number of needed sampling points for current integral
         * @param sector number of the sector in use
         * @param function number of the function in chosen sector in use
         * @return the needed maxeval
         */
        static long long balancedMaxeval(int sector, int function);

        /**
         * Run the integration via communicating with another process.
         * This function uses char*, not strings becase in works with pipes and it results can be used with MPI, strings would require an extra allocation
         * @param threadNumber The number of the process in the pool
         * @param integrand integrand to be integrates
         * @param currentMaxeval in case of balancing used to set the maxeval for this integral
         * @param result Result in { , , , } format
         * @return The number of sampling points used
         */
        static long long RunIntegration(int threadNumber, const char* integrand, long long currentMaxeval, char* result);

        /**
         * Prints a command to be used to get integrand file from input database
         * This function is called in case of crashes or incorrect answers from CIntegrate
         * @param sector number of the integrand sector
         * @param function number of the used function in sector if positive, otherwise all functions in a sector are used
         * @param res the answer from CIntegrate in case we have it
         * @return void
         */
        static void PrintToolCommand(int sector, int function, const std::string& res);

        /**
         * Runs a simple integration test verifying basic integrator correctness
         * Prints OK and integration parameters to stdout
         * Throws exception on any error.
         * @return void
         */
        static void RunIntegrationTest();

        inline static std::string integrationCommand = {}; /**<Stored command used to call pool. Saved in the output database*/
        inline static bool printIntegrationCommand = false; /**<Indication to print the integrtion command to stdout*/
        inline static std::string expandVar = "t"; /**<Name of the expansion variable*/
        inline static std::string integrator = "vegasCuba"; /**<The integrator to be passed to CIntegrate*/
        inline static std::string MPMin = "default"; /**<MPMin to be passed to CIntegrate (if not default)*/
        inline static std::string MPPrecisionShift = "default"; /**<MPPrecisionShift to be passed to CIntegrate (if not default)*/
        inline static std::string MPSmallX = "default"; /**<MPSmallX to be passed to CIntegrate (if not default)*/
        inline static std::string MPThreshold = "default"; /**<MPThreshold to be passed to CIntegrate (if not default)*/
        inline static std::map<std::string, std::string> intpar = {}; /**<List of integration parameters (name and value) to be passed to CIntegrate*/
        inline static std::string binaryPath = {}; /**<Path of the CIntegrate executable to be used. Not set by default, in this case detected automatically*/
        inline static std::string inputDatabaseName = {}; /**<Name of the input database*/
        inline static std::string outputDatabaseName = {}; /**<Name of the output database*/
        inline static std::string mathBinary = {}; /**<Path to mathematica binary for use by CINtegrate for evaluation of some functions, not set by default*/
        inline static int bucket = 10; /**<Kyotocabinet database tuning setting*/
        inline static int threadsNumber = 1; /**<Number of threads working in parallel with different integrands*/
        inline static bool testMode = false; /**<Indication that we should only perform a small test that CIntegrate works*/
        inline static bool printStatistics = false; /**<Whether to print statistics on sampling points*/
        inline static std::atomic<unsigned long long> pointsCount = {}; /**<Number of sampling points used for all integrands totally*/
        inline static std::atomic<unsigned long long> pointsMPFRCount = {}; /**<Number of sampling points used for all integrands totally (only MPFR)*/
        inline static std::atomic<unsigned long long> termsCount = {}; /**<Number of terms (individual integrands evaluated)*/
        inline static std::string inputPrefix = {}; /**<Special prefix requested from input. Not set by default, all prefixes are used in this case*/
        inline static bool direct = true; /**<Indication that we work without a call from Mathematica, so we should not put results in temporary files*/
        inline static bool complexMode = false; /**<Indication to call the complex version of CIntegrate (I can appear)*/
        inline static bool separateTerms = false; /**<Indication to pass different terms in a sector separately for integration*/
        inline static bool mpfr = false; /**<Passed to CIntegrate, indication that we should calculate everything with MPFR*/
        inline static bool native = false; /**<Passed to CIntegrate, indication that we should calculate everything with native arithmetics*/
        inline static bool noOptimization = false; /**<Passes to CIntegrate, indication ti switch off triad optimization*/
        inline static bool testF = false; /**<Special mode for CIntegrate, result is meaningless, but checks sign of F*/
        inline static bool debug = false; /**<Debug mode, print results of all integrals*/
        inline static bool NoAVX = false; /**<Switches off AVX optimizations*/
        inline static bool onlyPrepare = false; /**<Indication that we should only prepare the output database, not run the integration*/
        inline static std::pair<int,int> latest = {}; /**<Latest pair sent for evaluation. Used by the handler to print error in case of CIntegrate crash*/
        inline static Database inputDatabase = {}; /**<Handler of the database with input integrals*/
        inline static Database outputDatabase= {}; /**<Handler of the database with output integrals*/
        inline static std::string requestedTask = "all"; /**<Task requested for integration from input, all if not requested*/

        inline static bool gpuMode = false; /**<Indication to use GPU-build CIntegrate (but some of them may fall back to CPU*/

        inline static int GPUCore = 0; /**<The GPU Core to be used by a thread or MPI process, detected automatically based on GPUPerNode, GPUThreadsPerNode and id*/
        inline static int GPUMemoryPart = 1; /**<The GPU memory sectorPart to be used by a thread or MPI process, detected automatically based on GPUPerNode, GPUThreadsPerNode and id*/

        inline static int GPUThreadsPerNode = 65536; /**<The number of GPU-allowed threads per node, a big number by default, meaning all threads can work with GPU*/
        inline static int GPUPerNode = 1; /**<The number of GPU per node, 1 by default, meaning that all allowed GPU threads will use GPU number 0*/

        inline static std::string currentPrefix = {}; /**<Prefix used currently for integration*/
        inline static std::string currentTask = {}; /**<Task used currently for integration*/
        inline static bool currentOnlyParse = false; /**<Currently we are only preparsing the expressions*/
        inline static bool currentOnlyEstimate = false; /**<Currently we are estimating integrals with the use of less sampling points*/
        inline static std::string currentNumberOfVariables = {}; /**<Number of variables for current prefix, used in no separate terms mode, stored as a string because we need no integer value*/

        inline static std::set<std::string> prefixesSmallVariable = {}; /**<Possible prefix endings, not including order*/
        inline static std::map<std::pair<int, std::string>, std::pair<double, double>> results = {}; /**<Results and error estimates for an order and remaining prefix part, used for rejoining*/
        inline static std::map<std::pair<int, std::string>, std::pair<double, double>> resultsImaginary = {}; /**<Same with results for complex part of those*/
        inline static std::list<std::string> tasks = {}; /**<List of tasks to be integrated, for most modes there will be one "1" task*/
        inline static std::list<std::string> prefixes = {}; /**<List of prefixes to be integrated, all possible by default*/
        inline static bool preparse = false; /**<Whether to perform an only parse check first before integrating*/

        inline static std::condition_variable job = {}; /**<Continition variable for a job by a worker to start*/
        inline static std::condition_variable jobQueueFilled = {}; /**<Condition variable for a master to put more jobs in the queue*/
        inline static std::condition_variable rjob = {}; /**<Condition variable for a result ready*/
        inline static std::condition_variable rjobQueueFilled = {}; /**<Condition variable for slaves to put more answers in the queue*/
        inline static std::mutex queueMutex = {}; /**<Mutex for putting jobs in the queue and taking them*/
        inline static std::mutex rqueueMutex = {}; /**<Mutex for putting answers in the queue and taking them*/
        inline static std::queue<IntegrationTask> jobQueue = {}; /**<Queue of jobs*/
        inline static std::queue<IntegrationTask> rjobQueue = {}; /**<Queue of answers*/

        inline static std::atomic<bool> stopSubmitter = {}; /**< Whether submitter must be stopped */
        inline static std::thread submitterThread = {}; /**<Thread putting jobs in the queue*/
        inline static std::exception_ptr submitterError = {}; /**<Set if there was an error in submitter and we should stop stop integrating*/
        inline static std::atomic<bool> stopReceiver = {}; /**< Whether receiver must be stopped */
        inline static std::thread receiverThread = {}; /**<Thread taking answers from the queue*/
        inline static std::exception_ptr receiverError = {}; /**<Sey it there was an error in receiver and we should stop integrating*/

        inline static int sectorPart = 1; /**<The part, 1 up to sectorParts that is being integrated*/
        inline static int sectorParts = 1; /**<The number of parts each prefix is split. Default is 1. If it is more that one call the program consequently for all possible parts*/
        inline static int lastSector = 0; /**<The last sector number, coincides normally with the number of sectors, but can be changed by parts*/
        inline static int firstSector = 1; /**<The first sector number, 1 normally, but can be changes by parts*/

        inline static SaveMode resultsMode = SaveMode::none; /**<Whether to save intermediate results. If none, they are non saved, if current, removed after prefix run*/

        inline static std::vector<FILE*> pipeToCIntegrate = {}; /**<Pipes for writing data to CIntegrate processes*/
        inline static std::vector<FILE*> pipeFromCIntegrate = {}; /**<Pipes for reading data from CIntegrate processes*/
        inline static std::vector<pid_t> pidCIntegrate = {}; /**<Process ids of CIntegrate*/

        inline static unsigned int queueSize = 32; /**<Queue size for jobs, large sizes increase memory usage, small sizes might degrade performance, default is 32*/

        inline static bool balanceSamplingPoints = false; /**<Whether to first estimate integrals and then to use different number of sampling points for different integrals*/
        inline static unsigned int estimationPart = 100; /**<Send estimationPart times less points in estimation mode*/
        inline static unsigned int minimalSamplingPart = 100; /**<Use minimally minimalSamplingPart times less points when running the evaluation*/
        inline static std::unordered_map<std::pair<int, int>, double> estimatedIntegrals = {}; /**<Filled after estimation stage, used for balancing*/
        inline static BalanceMode balanceMode = BalanceMode::normError; /**<Balance mode used to detect the number of sampling points for final run*/
        inline static double balancePower= 0.5; /**<Power of fraction between the top and the current value used to detect the number of sampling points*/
};
